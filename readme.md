# Odaberi.com

Odaberi.com je hrvatska verzija stranice [ISideWith.com](http://isidewith.com),
dizajnirana kako bi bila fleksibilna i ponovno upotrebljiva kod novih izbora.

Uz ovaj paket pridružena je i trenutna baza pitanja i odgovora za stranke. 

## Preduvjeti

Za instalaciju potrebna je pravilno podešena instalacija Composera, PHP-a 
7.0+, PostgreSQL-a 9.5+ i Redisa.

## Postavljanje

* Preuzmite projekt (`git clone`) 
* Instalirajte Composer pakete (`composer install`)
* Kopirajte `.env.example` kao `.env` i popunite Facebook API ključevima
* Ubacite podatke u bazu - ili putem već postavljenog SQL dumpa ili, za praznu 
bazu, putem `php artisan migrate`.
* (Privremeni hack, ipak je projekt nastao u 36 sati :) ) U bazu ćete vjerovatno
trebati ubaciti vrijednosti korisnika sa ID 0 (za anonimno rješavanje) i stranke
s ID 0 (za neodlučne pri ispunjavanju upitnika). Ostali podaci su nebitni jer se
inače neće koristiti, a ovaj bug će biti ispravljen.
* Poslužite ohlađeno.
