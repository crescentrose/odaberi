<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PartyAnswer extends Model
{
    public function party() {
      return $this->belongsTo("App\Party");
    }

    public function question() {
      return $this->belongsTo("App\Question");
    }
}
