<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserAnswer extends Model
{
    public $timestamps = false;
    protected $fillable = ["user_response_id", "question_id", "answer"];
    protected $primaryKey = null;
    public $incrementing = false;

    public function response() {
      return $this->belongsTo("App\UserResponse");
    }
}
