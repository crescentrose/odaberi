<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserResult extends Model
{
    protected $fillable = ['response_id', 'party_id', 'count', 'agrees', 'percentage'];

    public function response() {
      return $this->belongsTo("App\UserResponse", "response_id");
    }

    public function party() {
      return $this->belongsTo("App\Party", "party_id");
    }
}
