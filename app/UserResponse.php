<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class UserResponse extends Model
{
    public function user() {
      return $this->belongsTo("App\User");
    }

    public function answers() {
      return $this->hasMany("App\UserAnswer");
    }

    public function results() {
      return $this->hasMany("App\UserResult", "response_id");
    }

    public function import_answers($r) {
        foreach ($r->input('question') as $question => $answer) {

          // convert to db-friendly data
          switch ($answer) {
            case 'a':
              $a = "agree";
              break;
            case 'd':
              $a = "disagree";
              break;
            case 'n':
            default:
              $a = "neither";
          }

          \App\UserAnswer::create([
            "user_response_id" => $this->id,
            "question_id" => $question,
            "answer" => $a
          ]);
        }

        // calculate the results...
        $results = DB::select("
          with counts as (
            select count(*) as agrees, party_id
            from user_answers
            left join party_answers
              on user_answers.question_id = party_answers.question_id
            where
              user_response_id = ?
              and party_answers.answer != 'neither'
              and (
                user_answers.answer = party_answers.answer
                or user_answers.answer = 'neither'
              )
            group by party_id
          ), totals as (
            select count(*), party_id as pid from party_answers
            where answer != 'neither'
            group by party_id
          )
          select count, agrees, id
          from totals
          left join counts on party_id = pid
          left join parties on parties.id = pid
          where count > 22", [$this->id]);

        foreach ($results as $result) {
          \App\UserResult::create([
            "response_id" => $this->id,
            "party_id" => $result->id,
            "count" => $result->count,
            "agrees" => $result->agrees,
            "percentage" => round($result->agrees / $result->count * 100, 2)
          ]);
        }
    }

}
