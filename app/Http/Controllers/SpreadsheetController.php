<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class SpreadsheetController extends Controller
{
    public function show() {
      $questions = \App\Question::orderBy('category_id', 'asc')->get();
      $parties = \App\Party::where('id', '>', '0')->orderBy('id', 'asc')->get();
      return view('spreadsheet', [
        'questions' => $questions,
        'parties' => $parties
      ]);
    }
}
