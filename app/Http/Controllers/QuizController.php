<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;

class QuizController extends Controller
{
    public function show(Request $r) {
      return view('test', [
        'parties' => \App\Party::where('id', '>', '0')->orderBy('name', 'asc')->get(),
        'categories' => \App\QuestionCategory::orderBy("title", "desc")->get()
      ]);
    }

    public function submit(Request $r) {
      // create a new response placeholder
      $response = new \App\UserResponse;
      $response->user_id = \Auth::user()->id ?? 0;
      $response->ip = $r->ip();
      $response->electoral_unit = $r->input('unit');
      $response->voting_for = $r->input('party');
      $response->save();

      // this is so that we can identify the response for users who aren't logged in
      session(['latest_response' => $response->id]);

      // import answers
      $response->import_answers($r);

      return redirect('/me');
    }
}
