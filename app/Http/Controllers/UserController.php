<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;

class UserController extends Controller
{
    public function show(Request $r) {
      // make sure we have something to show
      if (!\Auth::user() && !session()->get('latest_response', false)) {
        return redirect('/');
      }

      // if the user is logged in, find their latest response from the db, otherwise
      // try to load it from the session (anonymous quizes)
      if (\Auth::user()) {
        $latest = \App\UserResponse::where('user_id', \Auth::user()->id)->orderBy("created_at", "desc")->first();
        if (!$latest) {
          // make sure to tell the user to take the test if they're logged in and have none
          return view("me", [
            "empty" => true
          ]);
        }
      } else {
        $latest = \App\UserResponse::find(session()->get('latest_response', 0));
        if (!$latest) {
          return redirect('/');
        }
      }
      return view("me", [
        "empty" => false,
        "latest_answers" => $latest->answers()->get(),
        "latest" => $latest->results()->orderBy("percentage", "desc")->get()
      ]);
    }
}
