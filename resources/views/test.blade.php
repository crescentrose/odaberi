@extends("templates.master")
@section("main")
<script src='https://www.google.com/recaptcha/api.js'></script>
<div class="container">
  <h4>Kratke upute</h4>
  <p class="light"><ul>
    <li>Ispod vas su 46 tvrdnji s kojima se možete slagati, ne slagati ili biti neutralni (nije vas briga i bilo koji stav vam paše).</li>
    <li>Svaku stranku smo ocijenili sa "da", "ne" ili "nema stava" po svakom pitanju. Nakon ispunjavanja imati ćete opciju prikaza detaljnijih
      stavova stranaka koje ćete moći usporediti sa svojima klikom na ime stranke u popisu.</li>
    <li>Na kraju kviza se nalazi nekoliko izbornih pitanja na koje nije potrebno odgovoriti, ali će se upotrijebiti u svrhu procjene kvalitete programa
      i predviđanja rezultata izbora. <b>Poštujemo vašu privatnost</b> tako da će <b>svi vaši rezultati biti anonimizirani prije objave</b> bez obzira na način popunjavanja
      kviza i vaše odabire, ali ako ispunite taj dio kviza imajte na umu da ćemo te podatke vjerovatno prikazati na ovim stranicama.</li>
  </ul></p>
  <form action="/submit" method="post">
    @foreach ($categories as $category)
      <h5>{{ $category->title }}</h5>
      @foreach ($category->questions as $question)
      <div class="col s12"><b>{{ $question->question }}</b>
        <div class="row">
          <div class="col s12 m4">
            <input name="question[{{$question->id}}]" value="a" type="radio" id="question{{$question->id}}a" />
            <label for="question{{$question->id}}a">Slažem se</label>
          </div>
          <div class="col s12 m4">
            <input name="question[{{$question->id}}]" value="" type="radio" id="question{{$question->id}}n" checked/>
            <label for="question{{$question->id}}n">Nemam mišljenje</label>
          </div>
          <div class="col s12 m4">
            <input name="question[{{$question->id}}]" value="d" type="radio" id="question{{$question->id}}d" />
            <label for="question{{$question->id}}d">Ne slažem se</label>
          </div>
        </div>
      </div>
      @endforeach
    @endforeach
    <div class="col s12"><h5>Izborni dio</h5>
      <div class="row">
        <div class="input-field col s12 m6">
          <select name="unit">
            <option value="0" selected>Ne znam / ne želim reći</option>
            <option value="1">1. jedinica (sjeverozapadni dio Zagrebačke županije, dio centra i zapada Grada Zagreba)</option>
            <option value="2">2. jedinica (istočni dio Zagrebačke županije, Koprivničko-križevačka županija, Bjelovarsko-bilogorska županija i istočni dio Grada Zagreba)</option>
            <option value="3">3. jedinica (Krapinsko-zagorska županija, Varaždinska županija i Međimurska županija)</option>
            <option value="4">4. jedinica (Virovitičko-podravska županija i Osječko-baranjska županija)</option>
            <option value="5">5. jedinica (Požeško-slavonska županija, Brodsko-posavsku županija i Vukovarsko-srijemsku županija)</option>
            <option value="6">6. jedinica (jugoistočni dio Zagrebačke županije, Sisačko-moslavačka županija i jugoistočni dio Grada Zagreba)</option>
            <option value="7">7. jedinica (jugozapadni dio Zagrebačke županije, Karlovačka županija, istočni dio Primorsko-goranske županije i južni dio Grada Zagreba)</option>
            <option value="8">8. jedinica (Istarska županija i zapadni dio Primorsko-goranske županije - uključujući Rijeku)</option>
            <option value="9">9. jedinica (Ličko-senjska županija, Zadarska županija, Šibensko-kninska županija i sjeverni dio Splitsko-dalmatinske županije)</option>
            <option value="10">10. jedinica (južni dio Splitsko-dalmatinske županije - uključujući Split i Dubrovačko-neretvanska županija)</option>
            <option value="11">11. jedinica (dijaspora)</option>
            <option value="12">12. jedinica (manjine)</option>

          </select>
          <label>Vaša izborna jedinica</label>
        </div>
        <div class="input-field col s12 m6">
          <select class="icons" name="party">
            <option value="0" selected>Ne znam / ne želim reći / netko drugi</option>
            @foreach ($parties as $party)
              <option value="{{$party->id}}" data-icon="{{ $party->avatar }}" class="circle">{{$party->name}}</option>
            @endforeach
          </select>
          <label>Za koga ćete glasati?</label>
        </div>
      </div>
    </div>
    {{ csrf_field() }}
    <div class="center"><input type="submit" value="Izračunaj!" class="waves-effect waves-light btn-large"></input></div>
  </form>
</div>

@endsection
