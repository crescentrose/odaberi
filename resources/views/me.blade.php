@extends("templates.master")
@section("main")
<div class="container">
  <div class="section">
    <div class="row">
      <h4>@if (\Auth::user())
        {{ \Auth::user()->name }}
      @else
        Anonimni korisnik
      @endif</h4>
      <div class="subtitle"><i class="material-icons" style="font-size:14px;">lock_outline</i> Ova stranica dostupna je samo vama.</div>
      <p class="light">Ova stranica prikazuje vaš posljednji rezultat. Ako ste rješavali anonimno, rezultat će nakon nekog vremena biti izbrisan.
        Ako riješite test opet, rezultat će se zamijeniti novim - kako biste došli do svog rezultata od bilo koje stranice kliknite na "Moj profil" u
        gornjem desnom kutu.</p>
      @if ($empty)
        Trenutno nemamo podataka za vas. <a href="/test">Riješite kviz<a/>!
      @else
      <h5>Vaš zadnji rezultat</h5>
      <div class="subtitle">Kliknite na ime opcije za detalje.</div>
      <div class="results col sm12 m6">
      @foreach ($latest as $result)
        @if($loop->first)
          <div data-id="{{ $result->party->id }}" class="first graph">
            <span class="name">{{ $result->party->name }}</span>
            <span class="percentage">{{ $result->percentage }}%</span>
          </div>
        @else
          <div data-id="{{ $result->party->id }}" class="graph">
            <span class="name">{{ $result->party->name }}</span>
            <span class="percentage">{{ $result->percentage }}%</span></div>
        @endif
        <div class="bar" data-id="{{ $result->party->id }}" style="width: {{ round($result->percentage) }}%">&nbsp;</div>
      @endforeach
      </div>

      <div class="col sm12 m6">
        @foreach ($latest as $result)
          <div class="details" id="details-{{ $result->party->id }}">
            <h5>{{ $result->party->name }}</h5>
            <i class="tiny material-icons">language</i> <a href="{{ $result->party->url }}">{{ $result->party->url}}</a>
            <p class="light">Slažete se u <b>{{ $result->agrees }}</b> od {{ $result->count }} pitanja.</p>
            <table class="spreadsheet bordered">
              <tbody>
                <tr>
                  <th>Pitanje</th>
                  <td>Vaš odgovor</td>
                  <td>{{ $result->party->name }}</td>
                </tr>

              @foreach ($result->party->answers()->orderBy('question_id', 'asc')->get() as $party_answers)
              <tr>
                <th class="question">{{ $party_answers->question->question }}</th>
                <td class="{{ $user_answer = $latest_answers->where('question_id', $party_answers->question_id)->first()->answer }}">
                  @if ($user_answer == "agree")
                    Da
                  @elseif ($user_answer == "disagree")
                    Ne
                  @else
                    -
                  @endif
                </td>
                <td class="{{ $party_answers->answer }}">{{ $party_answers->detail }}</td>
              </tr>
              @endforeach
            </tbody>
          </table>
          </div>
        @endforeach
      </div>
      @endif
    </div>
  </div>
</div>
@endsection
