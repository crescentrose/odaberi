
<!DOCTYPE html>
<html lang="en">
<head>
  <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
  <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1.0"/>
  <title>Odaberi.com - biraj informirano</title>

  <!-- CSS  -->
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,700&subset=latin-ext" rel="stylesheet">
  <link href="/css/app.css" type="text/css" rel="stylesheet" media="screen,projection"/>
</head>
<body>
  <nav class="grey darken-4" role="navigation">
    <div class="nav-wrapper container"><a id="logo-container" href="/" class="brand-logo">Odaberi.com</a>
      <ul class="right hide-on-med-and-down">
        @if (\Auth::user())
          <li><a href="/me"><b>Moj profil</b></a></li>
        @else
          <li><a href="/auth/facebook">Prijava</a></li>
        @endif
        <li><a href="/about">O projektu</a></li>
        <li><a href="/spreadsheet">Tablica pitanja</a></li>
      </ul>
      <a href="#" data-activates="nav-mobile" class="button-collapse"><i class="material-icons">menu</i></a>
      <ul id="nav-mobile" class="side-nav" style="transform: translateX(0px);">
        @if (\Auth::user())
          <li><a href="/me"><b>Moj profil</b></a></li>
        @else
          <li><a href="/auth/facebook">Prijava</a></li>
        @endif
        <li><a href="/about">O projektu</a></li>
        <li><a href="/spreadsheet">Tablica pitanja</a></li>
      </ul>
    </div>
  </nav>
@yield("main")
  <footer class="page-footer grey darken-4">

    <div class="container">
      <div class="row">
      </div>
    </div>

    <div class="footer-copyright">
      <div class="container">
      Autor:  <span class="orange-text text-lighten-3" href="/about">Darwin</span>. Kontakt e-mail: <code>odaberi@420blaze.it</code>
      </div>
    </div>
  </footer>
  <!--  Scripts-->
  <script src="https://code.jquery.com/jquery-2.1.1.min.js"></script>
  <script src="/js/all.js"></script>
  <!-- Piwik -->
  <script type="text/javascript">
      var _paq = _paq || [];
      _paq.push(['trackPageView']);
      _paq.push(['enableLinkTracking']);
      (function() {
      var u="//crescentrose.piwikpro.com//";
      _paq.push(['setTrackerUrl', u+'piwik.php']);
      _paq.push(['setSiteId', 1]);
      var d=document, g=d.createElement('script'),
      s=d.getElementsByTagName('script')[0];
      g.type='text/javascript'; g.async=true;
      g.defer=true; g.src=u+'piwik.js'; s.parentNode.insertBefore(g,s);
      })();
      </script>
  <noscript><p>
    <img src="//crescentrose.piwikpro.com/piwik.php?idsite=1"
    style="border:0;" alt="" /></p>
    </noscript>
  <!-- End Piwik Code -->

  </body>
</html>
