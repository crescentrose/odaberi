@extends("templates.master")
@section("main")
<div class="section no-pad-bot" id="index-banner">
  <div class="container">
    <br><br>
    <h1 class="header center orange-text">Odaberi.com</h1>
    <div class="row center">
      <h5 class="header col s12 light">Saznajte koja vam je hrvatska politička stranka ideološki najbliža na predstojećim izborima u manje od 10 minuta.</h5>
    </div>
    @if (\Auth::user())
      <div class="row center">
        <a href="/test" class="btn-large waves-effect waves-light blue">Započni test</a>
      </div>
      <div class="row center">
        <a href="/auth/logout" class="btn-large btn-flat">Odjava ({{\Auth::user()->name}})</a>
      </div>
    @else
      <div class="row center">
        <a href="/auth/facebook" class="btn-large waves-effect waves-light blue">Krenite s Facebookom</a>
      </div>
      <div class="row center">
        <a href="/test"  class="btn-large btn-flat waves-effect waves-orange">Krenite anonimno</a>
      </div>
    @endif

    <br><br>
  </div>
</div>


<div class="container">
  <div class="section">
    <div class="row">
      <div class="col s12 m4">
        <div class="icon-block">
          <h5 class="center">Kako aplikacija funkcionira?</h5>
          <p class="light">Prošli smo kroz izborne programe najpopularnijih stranaka i koalicija i, po uzoru na
            <a href="http://isidewith.com">ISideWith.com</a>, saželi ih u 46 pitanja koja se odnose na društvenu,
            obrazovnu, poljoprivrednu, ekonomsku, vanjsku, unutarnju, zdravstvenu politiku i brigu o okolišu. </p>
          <p class="light">Po završetku kviza, aplikacija će vam predložiti (nadamo se točno) ideološki
            najbliže političke opcije i usporediti vaše stavove sa stavovima stranaka.</p>
        </div>
      </div>

      <div class="col s12 m4">
        <div class="icon-block">
          <h5 class="center">Otkuda dolaze podaci?</h5>
          <p class="light">Podaci su prikupljeni sa službenih stranica i službenih programa. Ako vaše stranke nema <a href="/spreadsheet">na popisu</a>
            ili su podaci nepotpuni (radi točnosti, uvjetujemo prikaz stranaka s barem 23 odgovorena pitanja) ili netočni, možete nam se obratiti e-mailom ili
            putem naše Facebook stranice. <i>Molimo vas da potkrijepite stavove člancima iz reputabilnih medija ili sa službenih web stranica, a ako ste predstavnik
            stranke molimo vas da šaljete poruke preko službenih mejlova ili Facebook stranica.</i></p>
        </div>
      </div>

      <div class="col s12 m4">
        <div class="icon-block">
          <h5 class="center">Tko je iza projekta?</h5>
          <p class="light">Autor projekta je web developer iz Rijeke frustriran činjenicom da je javnosti bitnija rasprava o ustašama i partizanima od
            pravih problema. Autor nije član nikakve stranke, niti ga nijedna stranka financira ili promovira.</p>
          <p class="light"> Ovaj projekt je nastao preko vikenda u nadi da će biti koristan, i unaprijed se ispričavam za bilo kakve pogreške te obećajem
            da su nenamjerne.
          </p>
        </div>
      </div>
    </div>

  </div>
</div>

@endsection
