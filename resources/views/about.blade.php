@extends("templates.master")
@section("main")
<div class="container">
  <div class="section">
    <div class="row">
      <h4>O Odaberi.com</h4>
      <p class="light">Odaberi.com je projekt nastao u 36 sati kako bi olakšao hrvatskim građanima odabir na
        predstojećim izborima. Odaberi.com se ne fokusira na dramatične izjave i licitiranje tko može ponuditi
        više radnih mjesta i podijeliti više novaca, nego na činjenice i stavove. Iako su bitni ekonomski i
        socijalni stavovi u većine političkih stranaka slični i populistički, još uvijek postoji dovoljno razlika kako
        bi ovakva aplikacija bila korisna.</p>
      <p class="light">Odaberi.com je nezavisan projekt, i ne plaća me nikakva politička ili nepolitička organizacija. Ako
      smatrate da vam je stranica bila korisna i želite izraziti zahvalnost, možete poslati <a href="https://streamtip.com/y/UCsIltn0840K8aNQOAOZfKdA">novčanu
        donaciju putem StreamTipa</a> (Bitcoinovi i tradicionalan novac su dobrodošli).</p>
      <h5>Prijave netočnih ili nedostajućih informacija</h5>
      <p class="light">
        Ako neka informacija nedostaje ili je kriva, molim vas da se javite Facebook porukom na <a href="https://www.facebook.com/odaberiapp/">stranicu aplikacije</a>
        ili putem maila na <code>odaberi@420blaze.it</code>. Ako predstavljate stranku, molim vas da šaljete mailove i poruke preko službenih kanala (mail sa službene
        domene, poruka sa službene stranice).
      </p>
      <h5>Što nakon izbora?</h5>
        <p class="light">Nakon izbora vidjet ćemo koliko smo bili točni i koliko su programi stranaka bili točni i uskladiti algoritam i pitanja do idućih izbora. Cilj je
        imati jedinstvenu bazu preko koje možete vidjeti koliko su točno programi bili provedeni. </p>
      <h5>Pozdravi i zahvale</h5>
      <p class="light">
        Zahvaljujem se <a href="https://vedran.miletic.net/">Vedranu Miletiću</a> na ideji i testiranju.
      </p>
      <p class="light">
        Bez ovih slobodnih softvera otvorenog koda ova aplikacija ne bi bila moguća: <a href="https://laravel.com/">Laravel</a>, <a href="http://materializecss.com/">Materialize CSS</a>,
        <a href="https://www.archlinux.org/">Arch Linux</a>, <a href="https://www.postgresql.org">PostgreSQL</a>, <a href="http://redis.io">Redis</a>.
      </p>
      <h5>Skini projekt</h5>
      <p class="light">Projekt će biti dostupan na <a href="https://gitlab.com/crescentrose/odaberi">GitLabu</a> kroz nekoliko dana.</p>
    </div>
  </div>
</div>

@endsection
