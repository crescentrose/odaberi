@extends("templates.master")
@section("main")
<div class="container">
  <div class="section">
    <div class="row">
      <div class="col s12">
        <h5>Tablica pitanja</h5>
        <p class="light">Prošli smo kroz programe većih stranaka i tražili odgovore na pitanja u kojima razne stranke imaju
        različita mišljenja. Iz svake tvrdnje za svaku stranku smo izvukli jednu od tri opcije: <i>da</i> - stranka se slaže
        s navedenom tvrdnjom, <i>ne</i> - izrijekom su naveli (u kampanji ili u programu) da se ne slažu s tom tvrdnjom ili
        <i>nema stava</i> - nismo mogli pronaći ni podatak za ni podatak protiv.</p>
        <p class="light">U slučaju kontradiktornih podataka (recimo, HDZ je u nekim člancima protiv traženja nafte u Jadranu, ali
          u njihovom progamu piše da je ta opcija otvorena dokle god se poštuju ekološki standardi), uzimali smo najnoviji podatak.</p>
        <p class="light">U nekim pitanjima, poput pravosuđa, skoro sve stranke se slažu u tome da treba smanjiti broj slučajeva, ubrzati rad
        sudova itd. Budući da su na takva pitanja odgovori skoro svugdje isti, takva pitanja nismo uključivali zbog lakše čitkosti.</p>
        <p class="light">Budući da ne možemo odgovoriti na neka kompleksnija pitanja sa samo "da" ili "ne", na nekim pitanjima smo
        dodali i detalje o predloženoj implementaciji. Recimo, iako i Nema Prodaje i Pametno zagovaraju smanjivanje poreza na dohodak, Pametno
        zagovara jedinstvenu stopu poreza ispod 1.000.000kn, a Nema Prodaje govori o širenju poreznih razreda i smanjivanju stopa. "Šminkanje" poreza
        na dohodak isključivo malim promjenama poreznih razreda nismo uključivali kao slaganje s navedenom tvrdnjom, jer se pitanje odnosi na smanjivanje svima,
        a ne samo nekima.</p>
        <p class="light">Stranke koje nemaju stavove na barem 50% pitanja su isključene iz testa, ali prikazane ovdje radi potpunosti.</p>
        <p class="light">Ova tablica je dosta velika, pa će možda biti teško čitljiva preko mobitela. Radimo na tome da bude preglednija!</p>
    </div>
    <div class="col s12">
      <div class="spreadsheet">
        <table class="bordered spreadsheet">
          <tbody>
            <tr class="header">
              <th data-field="question">Pitanje</th>
              @foreach ($parties as $party)
                <td data-field="party{{ $party->id }}" class="party">{{ $party->name }}</td>
              @endforeach
            </tr>
            @foreach($questions as $question)
            <tr>
              <th class="question">{{ $question->question }}</th>
              @foreach ($question->answers()->where('party_id', '>', '0')->orderBy('party_id', 'asc')->get() as $answer)
              <td class="{{ $answer->answer }}">{{ $answer->detail }}</td>
              @endforeach
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

@endsection
