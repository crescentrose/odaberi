// Enable clicking to reveal details about a party
$(".graph, .bar").click(function(e){

  id = $(e.target).data('id');
  $(".details").hide();
  $("#details-"+id).fadeToggle(200);

  // scroll to the details to draw user's attention to it (for mobile)
  if ($("#details-"+id).is(":visible")) {
    $('html, body').animate({
      scrollTop: $("#details-"+id).offset().top
    }, 400);
  }
});

// enable Materialize elements
$(document).ready(function() {
    $('select').material_select();
    $('.button-collapse').sideNav();
});
