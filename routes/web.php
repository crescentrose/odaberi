<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| This file is where you may define all of the routes that are handled
| by your application. Just tell Laravel the URIs it should respond
| to using a Closure or controller method. Build something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('/test', "QuizController@show");
Route::any('/submit', "QuizController@submit");

// facebook auth
Route::get('auth/facebook', 'Auth\AuthController@redirectToProvider');
Route::get('auth/facebook/callback', 'Auth\AuthController@handleProviderCallback');
Route::get('auth/logout', 'Auth\AuthController@logout');

Route::get('/spreadsheet', "SpreadsheetController@show");
Route::get('/me', "UserController@show");
Route::get('/about', function() {
  return view("about");
});
