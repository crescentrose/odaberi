--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.3
-- Dumped by pg_dump version 9.5.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: migrations; Type: TABLE; Schema: public; Owner: homestead
--

CREATE TABLE migrations (
    migration character varying(255) NOT NULL,
    batch integer NOT NULL
);


ALTER TABLE migrations OWNER TO homestead;

--
-- Name: parties; Type: TABLE; Schema: public; Owner: homestead
--

CREATE TABLE parties (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    avatar character varying(255),
    url character varying(255),
    description text,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE parties OWNER TO homestead;

--
-- Name: parties_id_seq; Type: SEQUENCE; Schema: public; Owner: homestead
--

CREATE SEQUENCE parties_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE parties_id_seq OWNER TO homestead;

--
-- Name: parties_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: homestead
--

ALTER SEQUENCE parties_id_seq OWNED BY parties.id;


--
-- Name: party_answers; Type: TABLE; Schema: public; Owner: homestead
--

CREATE TABLE party_answers (
    id integer NOT NULL,
    question_id integer NOT NULL,
    party_id integer NOT NULL,
    answer character varying(255) NOT NULL,
    detail character varying(255),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone,
    CONSTRAINT party_answers_answer_check CHECK (((answer)::text = ANY ((ARRAY['agree'::character varying, 'disagree'::character varying, 'neither'::character varying])::text[])))
);


ALTER TABLE party_answers OWNER TO homestead;

--
-- Name: party_answers_id_seq; Type: SEQUENCE; Schema: public; Owner: homestead
--

CREATE SEQUENCE party_answers_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE party_answers_id_seq OWNER TO homestead;

--
-- Name: party_answers_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: homestead
--

ALTER SEQUENCE party_answers_id_seq OWNED BY party_answers.id;


--
-- Name: password_resets; Type: TABLE; Schema: public; Owner: homestead
--

CREATE TABLE password_resets (
    email character varying(255) NOT NULL,
    token character varying(255) NOT NULL,
    created_at timestamp(0) without time zone
);


ALTER TABLE password_resets OWNER TO homestead;

--
-- Name: question_categories; Type: TABLE; Schema: public; Owner: homestead
--

CREATE TABLE question_categories (
    id integer NOT NULL,
    title character varying(255) NOT NULL,
    color character varying(255) NOT NULL
);


ALTER TABLE question_categories OWNER TO homestead;

--
-- Name: question_categories_id_seq; Type: SEQUENCE; Schema: public; Owner: homestead
--

CREATE SEQUENCE question_categories_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE question_categories_id_seq OWNER TO homestead;

--
-- Name: question_categories_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: homestead
--

ALTER SEQUENCE question_categories_id_seq OWNED BY question_categories.id;


--
-- Name: questions; Type: TABLE; Schema: public; Owner: homestead
--

CREATE TABLE questions (
    id integer NOT NULL,
    question character varying(255) NOT NULL,
    description text,
    category_id integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    deleted_at timestamp(0) without time zone
);


ALTER TABLE questions OWNER TO homestead;

--
-- Name: questions_id_seq; Type: SEQUENCE; Schema: public; Owner: homestead
--

CREATE SEQUENCE questions_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE questions_id_seq OWNER TO homestead;

--
-- Name: questions_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: homestead
--

ALTER SEQUENCE questions_id_seq OWNED BY questions.id;


--
-- Name: user_answers; Type: TABLE; Schema: public; Owner: homestead
--

CREATE TABLE user_answers (
    user_response_id integer NOT NULL,
    question_id integer NOT NULL,
    answer character varying(255) NOT NULL,
    CONSTRAINT user_answers_answer_check CHECK (((answer)::text = ANY ((ARRAY['agree'::character varying, 'disagree'::character varying, 'neither'::character varying])::text[])))
);


ALTER TABLE user_answers OWNER TO homestead;

--
-- Name: user_responses; Type: TABLE; Schema: public; Owner: homestead
--

CREATE TABLE user_responses (
    id integer NOT NULL,
    user_id integer,
    ip text NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone,
    electoral_unit integer,
    voting_for integer,
    top_result integer,
    result_percentage double precision
);


ALTER TABLE user_responses OWNER TO homestead;

--
-- Name: user_responses_id_seq; Type: SEQUENCE; Schema: public; Owner: homestead
--

CREATE SEQUENCE user_responses_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_responses_id_seq OWNER TO homestead;

--
-- Name: user_responses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: homestead
--

ALTER SEQUENCE user_responses_id_seq OWNED BY user_responses.id;


--
-- Name: user_results; Type: TABLE; Schema: public; Owner: homestead
--

CREATE TABLE user_results (
    id integer NOT NULL,
    response_id integer NOT NULL,
    party_id integer NOT NULL,
    percentage double precision NOT NULL,
    count integer NOT NULL,
    agrees integer NOT NULL,
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE user_results OWNER TO homestead;

--
-- Name: user_results_id_seq; Type: SEQUENCE; Schema: public; Owner: homestead
--

CREATE SEQUENCE user_results_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_results_id_seq OWNER TO homestead;

--
-- Name: user_results_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: homestead
--

ALTER SEQUENCE user_results_id_seq OWNED BY user_results.id;


--
-- Name: users; Type: TABLE; Schema: public; Owner: homestead
--

CREATE TABLE users (
    id integer NOT NULL,
    name character varying(255) NOT NULL,
    email character varying(255) NOT NULL,
    facebook_id character varying(255) NOT NULL,
    avatar character varying(255),
    remember_token character varying(100),
    created_at timestamp(0) without time zone,
    updated_at timestamp(0) without time zone
);


ALTER TABLE users OWNER TO homestead;

--
-- Name: users_id_seq; Type: SEQUENCE; Schema: public; Owner: homestead
--

CREATE SEQUENCE users_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE users_id_seq OWNER TO homestead;

--
-- Name: users_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: homestead
--

ALTER SEQUENCE users_id_seq OWNED BY users.id;


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: homestead
--

ALTER TABLE ONLY parties ALTER COLUMN id SET DEFAULT nextval('parties_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: homestead
--

ALTER TABLE ONLY party_answers ALTER COLUMN id SET DEFAULT nextval('party_answers_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: homestead
--

ALTER TABLE ONLY question_categories ALTER COLUMN id SET DEFAULT nextval('question_categories_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: homestead
--

ALTER TABLE ONLY questions ALTER COLUMN id SET DEFAULT nextval('questions_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: homestead
--

ALTER TABLE ONLY user_responses ALTER COLUMN id SET DEFAULT nextval('user_responses_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: homestead
--

ALTER TABLE ONLY user_results ALTER COLUMN id SET DEFAULT nextval('user_results_id_seq'::regclass);


--
-- Name: id; Type: DEFAULT; Schema: public; Owner: homestead
--

ALTER TABLE ONLY users ALTER COLUMN id SET DEFAULT nextval('users_id_seq'::regclass);


--
-- Data for Name: migrations; Type: TABLE DATA; Schema: public; Owner: homestead
--

COPY migrations (migration, batch) FROM stdin;
2014_10_12_000000_create_users_table	1
2014_10_12_100000_create_password_resets_table	1
2016_09_03_182614_AddParties	1
2016_09_03_182615_AddQuestionCategories	1
2016_09_03_182630_AddQuestions	1
2016_09_03_182705_AddPartyAnswers	1
2016_09_03_182730_AddUserResponses	1
2016_09_03_184453_AddUserAnswers	1
2016_09_04_000004_AddUserResponseDetails	2
2016_09_04_003602_AddUserResults	3
\.


--
-- Data for Name: parties; Type: TABLE DATA; Schema: public; Owner: homestead
--

COPY parties (id, name, avatar, url, description, created_at, updated_at, deleted_at) FROM stdin;
2	Narodna Koalicija (SDP - HNS - HSS - HSU)\n	\N	https://narodnakoalicija.hr/	\N	2016-09-03 20:26:47	2016-09-03 20:26:47	\N
3	HDZ	\N	http://hdz.hr/	\N	2016-09-03 20:27:08	2016-09-03 20:27:08	\N
4	Pametno	\N	http://www.pametno.org/	\N	2016-09-03 21:39:36	2016-09-03 21:39:36	\N
5	Živi Zid	\N	http://www.zivizid.hr/	\N	2016-09-03 21:49:36	2016-09-03 21:49:36	\N
6	Most Nezavisnih Lista	\N	http://most-nl.com/	\N	2016-09-03 21:55:26	2016-09-03 21:55:26	\N
7	HDSSB, HKS	\N	http://konzervativci.hr/	\N	2016-09-03 21:57:50	2016-09-03 21:57:50	\N
8	Domovinska koalicija (HSP-AS, HKDU, HDS, USP, Desno)	\N	http://www.desno.info/?tag=domovinska-koalicija	\N	2016-09-03 21:58:09	2016-09-03 21:58:09	\N
9	Nema prodaje (ORaH, SH, Zajedno, Stipe Petrina)	\N	http://nemaprodaje.hr	\N	2016-09-03 21:58:27	2016-09-03 21:58:27	\N
10	Bandić Milan 365 - Stranka rada i solidarnosti	\N	http://www.365ris.hr/	\N	2016-09-03 21:58:39	2016-09-03 21:58:39	\N
11	Hrvatski laburisti	\N	http://www.laburisti.hr	\N	2016-09-03 21:59:02	2016-09-03 21:59:02	\N
12	IDS, PGS, Lista za Rijeku	\N	http://www.ids-ddi.com/	\N	2016-09-03 21:59:17	2016-09-03 21:59:17	\N
\.


--
-- Name: parties_id_seq; Type: SEQUENCE SET; Schema: public; Owner: homestead
--

SELECT pg_catalog.setval('parties_id_seq', 8, true);


--
-- Data for Name: party_answers; Type: TABLE DATA; Schema: public; Owner: homestead
--

COPY party_answers (id, question_id, party_id, answer, detail, created_at, updated_at, deleted_at) FROM stdin;
140	3	4	agree	Da	2016-09-03 21:39:54	2016-09-03 21:39:54	\N
141	4	4	agree	Da	2016-09-03 21:39:54	2016-09-03 21:39:54	\N
142	5	4	neither	-	2016-09-03 21:39:54	2016-09-03 21:39:54	\N
143	6	4	neither	-	2016-09-03 21:39:54	2016-09-03 21:39:54	\N
144	7	4	neither	-	2016-09-03 21:39:54	2016-09-03 21:39:54	\N
145	8	4	agree	Da	2016-09-03 21:39:54	2016-09-03 21:39:54	\N
146	9	4	agree	Da	2016-09-03 21:39:54	2016-09-03 21:39:54	\N
147	10	4	agree	Da, povećanje udjela obnovljivih izvora energije	2016-09-03 21:39:54	2016-09-03 21:39:54	\N
148	11	4	agree	Da	2016-09-03 21:39:54	2016-09-03 21:39:54	\N
149	12	4	neither	-	2016-09-03 21:39:54	2016-09-03 21:39:54	\N
150	13	4	disagree	Ne	2016-09-03 21:39:54	2016-09-03 21:39:54	\N
151	14	4	agree	Da, dodjela vlasničkog kapitala.	2016-09-03 21:39:54	2016-09-03 21:39:54	\N
152	15	4	agree	Da, zajamčeni minimalni standard.	2016-09-03 21:39:54	2016-09-03 21:39:54	\N
153	16	4	disagree	Ne, takve mjere su samo vatrogasne	2016-09-03 21:39:54	2016-09-03 21:39:54	\N
154	17	4	agree	Da, jedinstvena stopa poreza ispod 1.000.000kn.	2016-09-03 21:39:54	2016-09-03 21:39:54	\N
155	18	4	agree	Da, ukidanje parafiskalnih nameta i jedinstvena stopa poreza na dobit	2016-09-03 21:39:54	2016-09-03 21:39:54	\N
156	19	4	disagree	Ne, ali poticati domaću proizvodnju	2016-09-03 21:39:54	2016-09-03 21:39:54	\N
157	20	4	neither	-	2016-09-03 21:39:54	2016-09-03 21:39:54	\N
158	21	4	agree	Da	2016-09-03 21:39:54	2016-09-03 21:39:54	\N
159	22	4	agree	Da, kroz kontrolu službenika.	2016-09-03 21:39:54	2016-09-03 21:39:54	\N
160	23	4	agree	Da	2016-09-03 21:39:54	2016-09-03 21:39:54	\N
161	24	4	agree	Da	2016-09-03 21:39:54	2016-09-03 21:39:54	\N
162	25	4	agree	Da	2016-09-03 21:39:54	2016-09-03 21:39:54	\N
163	26	4	neither	-	2016-09-03 21:39:54	2016-09-03 21:39:54	\N
164	27	4	neither	-	2016-09-03 21:39:54	2016-09-03 21:39:54	\N
165	28	4	disagree	Ne, ali dopustiti liječnicima da rade privatno ako žele.	2016-09-03 21:39:54	2016-09-03 21:39:54	\N
166	29	4	neither	-	2016-09-03 21:39:54	2016-09-03 21:39:54	\N
167	30	4	agree	Da	2016-09-03 21:39:54	2016-09-03 21:39:54	\N
168	31	4	agree	Da, kroz stipendije	2016-09-03 21:39:54	2016-09-03 21:39:54	\N
169	32	4	disagree	Ne	2016-09-03 21:39:54	2016-09-03 21:39:54	\N
170	33	4	agree	Da, za učenike s lošijim ekonomskim statusom	2016-09-03 21:39:54	2016-09-03 21:39:54	\N
171	34	4	neither	-	2016-09-03 21:39:54	2016-09-03 21:39:54	\N
172	35	4	agree	Da, revidirati Vatikanske ugovore	2016-09-03 21:39:54	2016-09-03 21:39:54	\N
173	36	4	neither	-	2016-09-03 21:39:54	2016-09-03 21:39:54	\N
174	37	4	neither	-	2016-09-03 21:39:54	2016-09-03 21:39:54	\N
175	38	4	disagree	Ne	2016-09-03 21:39:54	2016-09-03 21:39:54	\N
176	39	4	neither	-	2016-09-03 21:39:54	2016-09-03 21:39:54	\N
177	40	4	neither	-	2016-09-03 21:39:54	2016-09-03 21:39:54	\N
178	41	4	disagree	Ne	2016-09-03 21:39:54	2016-09-03 21:39:54	\N
48	3	3	disagree	Ne	2016-09-03 21:00:55	2016-09-03 21:00:55	\N
49	4	3	disagree	Ne, život počinje začećem	2016-09-03 21:00:55	2016-09-03 21:00:55	\N
50	5	3	agree	Da	2016-09-03 21:00:55	2016-09-03 21:00:55	\N
51	6	3	agree	Da	2016-09-03 21:00:55	2016-09-03 21:00:55	\N
52	7	3	disagree	Ne	2016-09-03 21:00:55	2016-09-03 21:00:55	\N
53	8	3	agree	Da, iako su mijenjali postav čim su došli na vlast	2016-09-03 21:00:55	2016-09-03 21:00:55	\N
54	9	3	disagree	Ne, vatikanski ugovori su temelji odnosa Hrvatske i Crkve	2016-09-03 21:00:55	2016-09-03 21:00:55	\N
55	10	3	disagree	Ne, treba poštovati postojeće ekološke standarde	2016-09-03 21:00:55	2016-09-03 21:00:55	\N
56	11	3	agree	Da	2016-09-03 21:00:55	2016-09-03 21:00:55	\N
57	12	3	disagree	Ne	2016-09-03 21:00:55	2016-09-03 21:00:55	\N
58	13	3	agree	Da, različita minimalna plaća po sektorima	2016-09-03 21:00:55	2016-09-03 21:00:55	\N
59	14	3	agree	Da, mala poduzeća mogu dobiti do 350.000 kn	2016-09-03 21:00:55	2016-09-03 21:00:55	\N
60	15	3	agree	Da, zajamčeni minimalni standard.	2016-09-03 21:00:55	2016-09-03 21:00:55	\N
61	16	3	agree	Da, svi mladi imaju garantiran posao	2016-09-03 21:00:55	2016-09-03 21:00:55	\N
62	17	3	agree	Da	2016-09-03 21:00:55	2016-09-03 21:00:55	\N
63	18	3	agree	Da, podići prag za ulaz u sustav PDV-a	2016-09-03 21:00:55	2016-09-03 21:00:55	\N
64	19	3	disagree	Ne, ali poticati domaću proizvodnju	2016-09-03 21:00:55	2016-09-03 21:00:55	\N
65	20	3	neither	-	2016-09-03 21:00:55	2016-09-03 21:00:55	\N
66	21	3	disagree	Ne, ali davati imovinu u koncesiju ili javno-privatno partnerstvo	2016-09-03 21:00:55	2016-09-03 21:00:55	\N
67	22	3	neither	-	2016-09-03 21:00:55	2016-09-03 21:00:55	\N
68	23	3	agree	Da	2016-09-03 21:00:55	2016-09-03 21:00:55	\N
69	24	3	neither	-	2016-09-03 21:00:55	2016-09-03 21:00:55	\N
70	25	3	agree	Da	2016-09-03 21:00:55	2016-09-03 21:00:55	\N
71	26	3	neither	-	2016-09-03 21:00:55	2016-09-03 21:00:55	\N
72	27	3	neither	-	2016-09-03 21:00:55	2016-09-03 21:00:55	\N
73	28	3	disagree	Ne	2016-09-03 21:00:55	2016-09-03 21:00:55	\N
74	29	3	disagree	Ne, pravo na zdravlje je temeljno ljudsko pravo	2016-09-03 21:00:55	2016-09-03 21:00:55	\N
75	30	3	agree	Da, iako su je rušili.	2016-09-03 21:00:55	2016-09-03 21:00:55	\N
76	31	3	agree	Da, poticati STEM područja	2016-09-03 21:00:55	2016-09-03 21:00:55	\N
77	32	3	disagree	Ne, ali uvesti dualno obrazovanje.	2016-09-03 21:00:55	2016-09-03 21:00:55	\N
78	33	3	agree	Da, za deficitarne struke	2016-09-03 21:00:55	2016-09-03 21:00:55	\N
79	34	3	neither	-	2016-09-03 21:00:55	2016-09-03 21:00:55	\N
80	35	3	disagree	Ne	2016-09-03 21:00:55	2016-09-03 21:00:55	\N
81	36	3	neither	-	2016-09-03 21:00:55	2016-09-03 21:00:55	\N
82	37	3	agree	Da	2016-09-03 21:00:55	2016-09-03 21:00:55	\N
83	38	3	disagree	Ne	2016-09-03 21:00:55	2016-09-03 21:00:55	\N
84	39	3	disagree	Ne	2016-09-03 21:00:55	2016-09-03 21:00:55	\N
85	40	3	neither	-	2016-09-03 21:00:55	2016-09-03 21:00:55	\N
86	41	3	neither	-	2016-09-03 21:00:55	2016-09-03 21:00:55	\N
87	42	3	disagree	Ne, ali decentralizirati upravljanje	2016-09-03 21:00:55	2016-09-03 21:00:55	\N
88	43	3	agree	Da, poboljšati e-upravu	2016-09-03 21:00:55	2016-09-03 21:00:55	\N
89	44	3	neither	-	2016-09-03 21:00:55	2016-09-03 21:00:55	\N
90	45	3	agree	Da	2016-09-03 21:00:55	2016-09-03 21:00:55	\N
91	46	3	agree	Da	2016-09-03 21:00:55	2016-09-03 21:00:55	\N
92	47	3	agree	Da	2016-09-03 21:00:55	2016-09-03 21:00:55	\N
93	48	3	agree	Da	2016-09-03 21:00:55	2016-09-03 21:00:55	\N
94	3	2	agree	Da	2016-09-03 21:09:09	2016-09-03 21:09:09	\N
95	4	2	neither	-	2016-09-03 21:09:09	2016-09-03 21:09:09	\N
96	5	2	agree	Da, dodijeliti besplatne stanove i opremiti veteranske centre.	2016-09-03 21:09:09	2016-09-03 21:09:09	\N
97	6	2	agree	Da, omogućiti zaštićen mjesečni iznos.	2016-09-03 21:09:09	2016-09-03 21:09:09	\N
98	7	2	neither	-	2016-09-03 21:09:09	2016-09-03 21:09:09	\N
99	8	2	agree	Da	2016-09-03 21:09:09	2016-09-03 21:09:09	\N
100	9	2	neither	-	2016-09-03 21:09:09	2016-09-03 21:09:09	\N
101	10	2	agree	Da, putem ulaganja u obnovljive izvore energije	2016-09-03 21:09:09	2016-09-03 21:09:09	\N
102	11	2	agree	Da	2016-09-03 21:09:09	2016-09-03 21:09:09	\N
103	12	2	neither	-	2016-09-03 21:09:09	2016-09-03 21:09:09	\N
104	13	2	agree	Da, ukidanjem nekih davanja.	2016-09-03 21:09:09	2016-09-03 21:09:09	\N
105	14	2	disagree	Ne, ali smanjiti prepreke i olakšati pokretanje poduzeća.	2016-09-03 21:09:09	2016-09-03 21:09:09	\N
106	15	2	agree	Da, zajamčeni minimalni standard.	2016-09-03 21:09:09	2016-09-03 21:09:09	\N
107	16	2	agree	Da (Garancija za mlade)	2016-09-03 21:09:09	2016-09-03 21:09:09	\N
108	17	2	disagree	Ne, ali promijeniti porezne razrede.	2016-09-03 21:09:09	2016-09-03 21:09:09	\N
109	18	2	agree	Da, smanjiti opterećenje malim poduzetnicima	2016-09-03 21:09:09	2016-09-03 21:09:09	\N
110	19	2	disagree	Ne, ali poticati domaću proizvodnju	2016-09-03 21:09:09	2016-09-03 21:09:09	\N
111	20	2	neither	-	2016-09-03 21:09:09	2016-09-03 21:09:09	\N
112	21	2	disagree	Ne, ali konsolidirati i racionalnije upravljati.	2016-09-03 21:09:09	2016-09-03 21:09:09	\N
113	22	2	neither	-	2016-09-03 21:09:09	2016-09-03 21:09:09	\N
114	23	2	agree	Da	2016-09-03 21:09:09	2016-09-03 21:09:09	\N
115	24	2	neither	-	2016-09-03 21:09:09	2016-09-03 21:09:09	\N
116	25	2	agree	Da	2016-09-03 21:09:09	2016-09-03 21:09:09	\N
117	26	2	neither	-	2016-09-03 21:09:09	2016-09-03 21:09:09	\N
118	27	2	neither	-	2016-09-03 21:09:09	2016-09-03 21:09:09	\N
119	28	2	disagree	Ne, HZZO treba biti jedinstvena javna ustanova.	2016-09-03 21:09:09	2016-09-03 21:09:09	\N
120	29	2	disagree	Ne, smanjiti participacije.	2016-09-03 21:09:09	2016-09-03 21:09:09	\N
121	30	2	agree	Da	2016-09-03 21:09:09	2016-09-03 21:09:09	\N
122	31	2	agree	Da	2016-09-03 21:09:09	2016-09-03 21:09:09	\N
123	32	2	disagree	Ne	2016-09-03 21:09:09	2016-09-03 21:09:09	\N
124	33	2	agree	Da, samo za učenike s lošijim ekonomskim statusom	2016-09-03 21:09:09	2016-09-03 21:09:09	\N
125	34	2	agree	Da, u sklopu reforme	2016-09-03 21:09:09	2016-09-03 21:09:09	\N
126	35	2	neither	-	2016-09-03 21:09:09	2016-09-03 21:09:09	\N
127	36	2	neither	-	2016-09-03 21:09:09	2016-09-03 21:09:09	\N
128	37	2	agree	Da	2016-09-03 21:09:09	2016-09-03 21:09:09	\N
129	38	2	disagree	Ne	2016-09-03 21:09:09	2016-09-03 21:09:09	\N
130	39	2	agree	Da	2016-09-03 21:09:09	2016-09-03 21:09:09	\N
131	40	2	disagree	Ne	2016-09-03 21:09:09	2016-09-03 21:09:09	\N
132	41	2	neither	-	2016-09-03 21:09:09	2016-09-03 21:09:09	\N
133	42	2	disagree	Ne, ali decentralizirati upravljanje.	2016-09-03 21:09:09	2016-09-03 21:09:09	\N
134	43	2	agree	Da, nastaviti projekt e-građana	2016-09-03 21:09:09	2016-09-03 21:09:09	\N
135	44	2	agree	Da	2016-09-03 21:09:09	2016-09-03 21:09:09	\N
136	45	2	agree	Da, uz ograničen ulov i opremu.	2016-09-03 21:09:09	2016-09-03 21:09:09	\N
137	46	2	agree	Da	2016-09-03 21:09:09	2016-09-03 21:09:09	\N
138	47	2	agree	Da	2016-09-03 21:09:09	2016-09-03 21:09:09	\N
139	48	2	agree	Da	2016-09-03 21:09:09	2016-09-03 21:09:09	\N
179	42	4	agree	Da, i decentralizirati upravljanje	2016-09-03 21:39:54	2016-09-03 21:39:54	\N
180	43	4	agree	Da, puna funkcionalnost uključujući izbore preko interneta.	2016-09-03 21:39:54	2016-09-03 21:39:54	\N
181	44	4	disagree	Ne, i poticati biotehnologiju u proizvodnji hrane	2016-09-03 21:39:54	2016-09-03 21:39:54	\N
182	45	4	agree	Da, uz održivo upravljanje ribljim fondom	2016-09-03 21:39:54	2016-09-03 21:39:54	\N
183	46	4	agree	Da	2016-09-03 21:39:54	2016-09-03 21:39:54	\N
184	47	4	agree	Da	2016-09-03 21:39:54	2016-09-03 21:39:54	\N
185	48	4	agree	Da	2016-09-03 21:39:54	2016-09-03 21:39:54	\N
186	3	5	neither	-	2016-09-03 21:49:40	2016-09-03 21:49:40	\N
187	4	5	neither	-	2016-09-03 21:49:40	2016-09-03 21:49:40	\N
188	5	5	neither	-	2016-09-03 21:49:40	2016-09-03 21:49:40	\N
189	6	5	agree	Da, ovršni zakon je nepravedan.	2016-09-03 21:49:40	2016-09-03 21:49:40	\N
190	7	5	disagree	Ne	2016-09-03 21:49:40	2016-09-03 21:49:40	\N
191	8	5	neither	-	2016-09-03 21:49:40	2016-09-03 21:49:40	\N
192	9	5	agree	Da, ukidanje vatikanskih ugovora.	2016-09-03 21:49:40	2016-09-03 21:49:40	\N
193	10	5	agree	Da	2016-09-03 21:49:40	2016-09-03 21:49:40	\N
194	11	5	neither	-	2016-09-03 21:49:40	2016-09-03 21:49:40	\N
195	12	5	disagree	Ne	2016-09-03 21:49:40	2016-09-03 21:49:40	\N
196	13	5	agree	Da (3.500 kn)	2016-09-03 21:49:40	2016-09-03 21:49:40	\N
197	14	5	disagree	Ne	2016-09-03 21:49:40	2016-09-03 21:49:40	\N
198	15	5	neither	-	2016-09-03 21:49:40	2016-09-03 21:49:40	\N
199	16	5	neither	-	2016-09-03 21:49:40	2016-09-03 21:49:40	\N
200	17	5	agree	Da	2016-09-03 21:49:40	2016-09-03 21:49:40	\N
201	18	5	agree	Da	2016-09-03 21:49:40	2016-09-03 21:49:40	\N
202	19	5	agree	Da	2016-09-03 21:49:40	2016-09-03 21:49:40	\N
203	20	5	disagree	Ne	2016-09-03 21:49:40	2016-09-03 21:49:40	\N
204	21	5	disagree	Ne, povećati.	2016-09-03 21:49:40	2016-09-03 21:49:40	\N
206	23	5	disagree	Ne, vanjske investicije su samo preuzimanja uspješnih firmi.	2016-09-03 21:49:40	2016-09-03 21:49:40	\N
207	24	5	disagree	Ne	2016-09-03 21:49:40	2016-09-03 21:49:40	\N
208	25	5	disagree	Ne, EU treba reformirati.	2016-09-03 21:49:40	2016-09-03 21:49:40	\N
209	26	5	agree	Da, i poticati uzgoj	2016-09-03 21:49:40	2016-09-03 21:49:40	\N
210	27	5	disagree	Ne	2016-09-03 21:49:40	2016-09-03 21:49:40	\N
211	28	5	disagree	Ne, zdravstvo mora biti neprofitno	2016-09-03 21:49:40	2016-09-03 21:49:40	\N
212	29	5	agree	Da, dokle god je transparentno i pošteno (imovinski cenzus)	2016-09-03 21:49:40	2016-09-03 21:49:40	\N
213	30	5	neither	-	2016-09-03 21:49:40	2016-09-03 21:49:40	\N
214	31	5	disagree	Ne, svaki predmet je jednako bitan	2016-09-03 21:49:40	2016-09-03 21:49:40	\N
215	32	5	disagree	Ne, visoko školstvo mora biti bezuvjetno besplatno.	2016-09-03 21:49:40	2016-09-03 21:49:40	\N
216	33	5	disagree	Ne, ali svi studenti trebaju imati jednake uvjete.	2016-09-03 21:49:40	2016-09-03 21:49:40	\N
217	34	5	agree	Da	2016-09-03 21:49:40	2016-09-03 21:49:40	\N
218	35	5	disagree	Ne, ali vjeronauk treba biti izvannastavna aktivnost	2016-09-03 21:49:40	2016-09-03 21:49:40	\N
219	36	5	neither	-	2016-09-03 21:49:40	2016-09-03 21:49:40	\N
220	37	5	agree	Da	2016-09-03 21:49:40	2016-09-03 21:49:40	\N
221	38	5	agree	Da (NATO), i zalagati se za preustroj EU.	2016-09-03 21:49:40	2016-09-03 21:49:40	\N
222	39	5	neither	-	2016-09-03 21:49:40	2016-09-03 21:49:40	\N
223	40	5	neither	-	2016-09-03 21:49:40	2016-09-03 21:49:40	\N
224	41	5	neither	-	2016-09-03 21:49:40	2016-09-03 21:49:40	\N
225	42	5	neither	-	2016-09-03 21:49:40	2016-09-03 21:49:40	\N
226	43	5	neither	-	2016-09-03 21:49:40	2016-09-03 21:49:40	\N
227	44	5	agree	Da	2016-09-03 21:49:40	2016-09-03 21:49:40	\N
228	45	5	agree	Da	2016-09-03 21:49:40	2016-09-03 21:49:40	\N
229	46	5	agree	Da	2016-09-03 21:49:40	2016-09-03 21:49:40	\N
230	47	5	agree	Da	2016-09-03 21:49:40	2016-09-03 21:49:40	\N
231	48	5	agree	Da	2016-09-03 21:49:40	2016-09-03 21:49:40	\N
232	3	6	agree	Da	2016-09-03 21:56:29	2016-09-03 21:56:29	\N
233	4	6	disagree	Ne	2016-09-03 21:56:29	2016-09-03 21:56:29	\N
234	5	6	agree	Da, definirati položaj Ustavom.	2016-09-03 21:56:29	2016-09-03 21:56:29	\N
235	6	6	neither	-	2016-09-03 21:56:29	2016-09-03 21:56:29	\N
236	7	6	neither	-	2016-09-03 21:56:29	2016-09-03 21:56:29	\N
237	8	6	agree	Da, iako su omogućili HDZ-u da ruši neovisnost HRT-a.	2016-09-03 21:56:29	2016-09-03 21:56:29	\N
238	9	6	neither	-	2016-09-03 21:56:29	2016-09-03 21:56:29	\N
239	10	6	agree	Da	2016-09-03 21:56:29	2016-09-03 21:56:29	\N
240	11	6	agree	Da	2016-09-03 21:56:29	2016-09-03 21:56:29	\N
241	12	6	agree	Da, načelno nisu protiv.	2016-09-03 21:56:29	2016-09-03 21:56:29	\N
242	13	6	disagree	Ne	2016-09-03 21:56:29	2016-09-03 21:56:29	\N
243	14	6	disagree	Ne, ali smanjiti porezno opterećenje.	2016-09-03 21:56:29	2016-09-03 21:56:29	\N
244	15	6	neither	-	2016-09-03 21:56:29	2016-09-03 21:56:29	\N
278	3	7	disagree	Ne, obitelj su muškarac i žena	2016-09-03 22:01:13	2016-09-03 22:01:13	\N
245	16	6	disagree	Ne, ali mladima treba omogućiti osnovanje j.d.o.o. bez gubitka prava na redovno školovanje	2016-09-03 21:56:29	2016-09-03 21:56:29	\N
246	17	6	disagree	Ne, ali promijeniti porezne razrede.	2016-09-03 21:56:29	2016-09-03 21:56:29	\N
247	18	6	agree	Da, ukidanje poreza na dividendu i drugih nameta.	2016-09-03 21:56:29	2016-09-03 21:56:29	\N
248	19	6	disagree	Ne, ali poticati domaću proizvodnju	2016-09-03 21:56:29	2016-09-03 21:56:29	\N
249	20	6	neither	-	2016-09-03 21:56:29	2016-09-03 21:56:29	\N
250	21	6	neither	-	2016-09-03 21:56:29	2016-09-03 21:56:29	\N
251	22	6	agree	Da, kroz smanjenje broja agencija i drugih javnih tijela.	2016-09-03 21:56:29	2016-09-03 21:56:29	\N
252	23	6	neither	-	2016-09-03 21:56:29	2016-09-03 21:56:29	\N
253	24	6	disagree	Ne	2016-09-03 21:56:29	2016-09-03 21:56:29	\N
254	25	6	agree	Da	2016-09-03 21:56:29	2016-09-03 21:56:29	\N
255	26	6	neither	-	2016-09-03 21:56:29	2016-09-03 21:56:29	\N
256	27	6	neither	-	2016-09-03 21:56:29	2016-09-03 21:56:29	\N
257	28	6	disagree	Ne, ali depolitizirati HZZO i omogućiti realno određivanje cijena.	2016-09-03 21:56:29	2016-09-03 21:56:29	\N
258	29	6	disagree	Ne, pravo na zdravlje je temeljno ljudsko pravo	2016-09-03 21:56:29	2016-09-03 21:56:29	\N
259	30	6	agree	Da, iako su je rušili.	2016-09-03 21:56:29	2016-09-03 21:56:29	\N
260	31	6	agree	Da, kroz stipendije	2016-09-03 21:56:29	2016-09-03 21:56:29	\N
261	32	6	disagree	Ne	2016-09-03 21:56:29	2016-09-03 21:56:29	\N
262	33	6	agree	Da, za učenike s lošijim ekonomskim statusom	2016-09-03 21:56:29	2016-09-03 21:56:29	\N
263	34	6	neither	-	2016-09-03 21:56:29	2016-09-03 21:56:29	\N
264	35	6	neither	-	2016-09-03 21:56:29	2016-09-03 21:56:29	\N
265	36	6	neither	-	2016-09-03 21:56:29	2016-09-03 21:56:29	\N
266	37	6	agree	Da	2016-09-03 21:56:29	2016-09-03 21:56:29	\N
267	38	6	neither	-	2016-09-03 21:56:29	2016-09-03 21:56:29	\N
268	39	6	neither	-	2016-09-03 21:56:29	2016-09-03 21:56:29	\N
269	40	6	disagree	Ne, osim u slučaju potrebe.	2016-09-03 21:56:29	2016-09-03 21:56:29	\N
270	41	6	neither	-	2016-09-03 21:56:29	2016-09-03 21:56:29	\N
271	42	6	agree	Da, i reformirati lokalnu samoupravu.	2016-09-03 21:56:29	2016-09-03 21:56:29	\N
272	43	6	agree	Da, nastaviti projekt e-građana	2016-09-03 21:56:29	2016-09-03 21:56:29	\N
273	44	6	neither	-	2016-09-03 21:56:29	2016-09-03 21:56:29	\N
274	45	6	neither	-	2016-09-03 21:56:29	2016-09-03 21:56:29	\N
275	46	6	agree	Da	2016-09-03 21:56:29	2016-09-03 21:56:29	\N
276	47	6	neither	-	2016-09-03 21:56:29	2016-09-03 21:56:29	\N
277	48	6	agree	Da	2016-09-03 21:56:29	2016-09-03 21:56:29	\N
279	4	7	disagree	Ne, život počinje začećem, obitelj su muškarac i žena	2016-09-03 22:01:13	2016-09-03 22:01:13	\N
280	5	7	agree	Da	2016-09-03 22:01:13	2016-09-03 22:01:13	\N
281	6	7	neither	-	2016-09-03 22:01:13	2016-09-03 22:01:13	\N
282	7	7	neither	-	2016-09-03 22:01:13	2016-09-03 22:01:13	\N
283	8	7	neither	-	2016-09-03 22:01:13	2016-09-03 22:01:13	\N
284	9	7	disagree	Ne	2016-09-03 22:01:13	2016-09-03 22:01:13	\N
285	10	7	neither	-	2016-09-03 22:01:13	2016-09-03 22:01:13	\N
286	11	7	agree	Da, Hrvatska mora biti energetski neovisna	2016-09-03 22:01:13	2016-09-03 22:01:13	\N
287	12	7	neither	-	2016-09-03 22:01:13	2016-09-03 22:01:13	\N
288	13	7	neither	-	2016-09-03 22:01:13	2016-09-03 22:01:13	\N
289	14	7	neither	-	2016-09-03 22:01:13	2016-09-03 22:01:13	\N
290	15	7	agree	Da, dokle god ne ugrožava financijsku stabilnost države.	2016-09-03 22:01:13	2016-09-03 22:01:13	\N
291	16	7	neither	-	2016-09-03 22:01:13	2016-09-03 22:01:13	\N
292	17	7	agree	Da, porezi moraju biti jednostavniji zbog konkurentnosti.	2016-09-03 22:01:13	2016-09-03 22:01:13	\N
293	18	7	agree	Da, nameti moraju biti manji i svrhovitiji, a porezi jednostavniji	2016-09-03 22:01:13	2016-09-03 22:01:13	\N
294	19	7	disagree	Ne, tržište mora biti slobodno	2016-09-03 22:01:13	2016-09-03 22:01:13	\N
295	20	7	neither	-	2016-09-03 22:01:13	2016-09-03 22:01:13	\N
296	21	7	neither	-	2016-09-03 22:01:13	2016-09-03 22:01:13	\N
297	22	7	agree	Da	2016-09-03 22:01:13	2016-09-03 22:01:13	\N
298	23	7	neither	-	2016-09-03 22:01:13	2016-09-03 22:01:13	\N
299	24	7	neither	-	2016-09-03 22:01:13	2016-09-03 22:01:13	\N
300	25	7	agree	Da	2016-09-03 22:01:13	2016-09-03 22:01:13	\N
301	26	7	neither	-	2016-09-03 22:01:13	2016-09-03 22:01:13	\N
302	27	7	neither	-	2016-09-03 22:01:13	2016-09-03 22:01:13	\N
303	28	7	neither	-	2016-09-03 22:01:13	2016-09-03 22:01:13	\N
304	29	7	disagree	Ne	2016-09-03 22:01:13	2016-09-03 22:01:13	\N
305	30	7	disagree	Ne	2016-09-03 22:01:13	2016-09-03 22:01:13	\N
306	31	7	neither	-	2016-09-03 22:01:13	2016-09-03 22:01:13	\N
307	32	7	neither	-	2016-09-03 22:01:13	2016-09-03 22:01:13	\N
308	33	7	neither	-	2016-09-03 22:01:13	2016-09-03 22:01:13	\N
309	34	7	disagree	Ne	2016-09-03 22:01:13	2016-09-03 22:01:13	\N
310	35	7	disagree	Ne	2016-09-03 22:01:13	2016-09-03 22:01:13	\N
311	36	7	agree	Da	2016-09-03 22:01:13	2016-09-03 22:01:13	\N
312	37	7	agree	Da	2016-09-03 22:01:13	2016-09-03 22:01:13	\N
313	38	7	disagree	Ne	2016-09-03 22:01:13	2016-09-03 22:01:13	\N
314	39	7	disagree	Ne	2016-09-03 22:01:13	2016-09-03 22:01:13	\N
315	40	7	neither	-	2016-09-03 22:01:13	2016-09-03 22:01:13	\N
316	41	7	neither	-	2016-09-03 22:01:13	2016-09-03 22:01:13	\N
317	42	7	agree	Da, i decentralizirati upravljanje	2016-09-03 22:01:13	2016-09-03 22:01:13	\N
318	43	7	agree	Da	2016-09-03 22:01:13	2016-09-03 22:01:13	\N
319	44	7	agree	Da	2016-09-03 22:01:13	2016-09-03 22:01:13	\N
320	45	7	neither	-	2016-09-03 22:01:13	2016-09-03 22:01:13	\N
321	46	7	agree	Da	2016-09-03 22:01:13	2016-09-03 22:01:13	\N
322	47	7	neither	-	2016-09-03 22:01:13	2016-09-03 22:01:13	\N
323	48	7	neither	-	2016-09-03 22:01:13	2016-09-03 22:01:13	\N
324	3	8	disagree	Ne, obitelj su muškarac i žena	2016-09-03 22:04:31	2016-09-03 22:04:31	\N
325	4	8	disagree	Ne, život počinje začećem, obitelj su muškarac i žena	2016-09-03 22:04:31	2016-09-03 22:04:31	\N
326	5	8	agree	Da	2016-09-03 22:04:31	2016-09-03 22:04:31	\N
327	6	8	neither	-	2016-09-03 22:04:31	2016-09-03 22:04:31	\N
328	7	8	neither	-	2016-09-03 22:04:31	2016-09-03 22:04:31	\N
329	8	8	neither	-	2016-09-03 22:04:31	2016-09-03 22:04:31	\N
330	9	8	disagree	Ne, Europa ima dominantni kršćanski identitet	2016-09-03 22:04:31	2016-09-03 22:04:31	\N
331	10	8	agree	Da, očuvati prirodu.	2016-09-03 22:04:31	2016-09-03 22:04:31	\N
332	11	8	neither	-	2016-09-03 22:04:31	2016-09-03 22:04:31	\N
333	12	8	neither	-	2016-09-03 22:04:31	2016-09-03 22:04:31	\N
334	13	8	neither	-	2016-09-03 22:04:31	2016-09-03 22:04:31	\N
335	14	8	agree	Da	2016-09-03 22:04:31	2016-09-03 22:04:31	\N
336	15	8	neither	-	2016-09-03 22:04:31	2016-09-03 22:04:31	\N
337	16	8	agree	Da, u sklopu ministarstva demografske obnove	2016-09-03 22:04:31	2016-09-03 22:04:31	\N
338	17	8	neither	-	2016-09-03 22:04:31	2016-09-03 22:04:31	\N
339	18	8	agree	Da	2016-09-03 22:04:31	2016-09-03 22:04:31	\N
340	19	8	agree	Da	2016-09-03 22:04:31	2016-09-03 22:04:31	\N
341	20	8	neither	-	2016-09-03 22:04:31	2016-09-03 22:04:31	\N
342	21	8	neither	-	2016-09-03 22:04:31	2016-09-03 22:04:31	\N
343	22	8	agree	Da, kroz provedbu lustracije.	2016-09-03 22:04:31	2016-09-03 22:04:31	\N
344	23	8	disagree	Ne, Hrvatska mora biti nezavisna	2016-09-03 22:04:31	2016-09-03 22:04:31	\N
345	24	8	disagree	Ne, Hrvatska mora biti nezavisna	2016-09-03 22:04:31	2016-09-03 22:04:31	\N
346	25	8	agree	Da	2016-09-03 22:04:31	2016-09-03 22:04:31	\N
347	26	8	neither	-	2016-09-03 22:04:31	2016-09-03 22:04:31	\N
348	27	8	neither	-	2016-09-03 22:04:31	2016-09-03 22:04:31	\N
349	28	8	neither	-	2016-09-03 22:04:31	2016-09-03 22:04:31	\N
350	29	8	disagree	Ne	2016-09-03 22:04:31	2016-09-03 22:04:31	\N
351	30	8	disagree	Ne	2016-09-03 22:04:31	2016-09-03 22:04:31	\N
352	31	8	neither	-	2016-09-03 22:04:31	2016-09-03 22:04:31	\N
353	32	8	neither	-	2016-09-03 22:04:31	2016-09-03 22:04:31	\N
354	33	8	neither	-	2016-09-03 22:04:31	2016-09-03 22:04:31	\N
355	34	8	disagree	Ne	2016-09-03 22:04:31	2016-09-03 22:04:31	\N
356	35	8	disagree	Ne, Europa ima dominantni kršćanski identitet	2016-09-03 22:04:31	2016-09-03 22:04:31	\N
357	36	8	agree	Da	2016-09-03 22:04:31	2016-09-03 22:04:31	\N
358	37	8	agree	Da	2016-09-03 22:04:31	2016-09-03 22:04:31	\N
359	38	8	disagree	Ne, ali zadržati što veći stupanj nezavisnosti	2016-09-03 22:04:31	2016-09-03 22:04:31	\N
360	39	8	disagree	Ne	2016-09-03 22:04:31	2016-09-03 22:04:31	\N
361	40	8	agree	Da	2016-09-03 22:04:31	2016-09-03 22:04:31	\N
362	41	8	neither	-	2016-09-03 22:04:31	2016-09-03 22:04:31	\N
363	42	8	neither	-	2016-09-03 22:04:31	2016-09-03 22:04:31	\N
364	43	8	neither	-	2016-09-03 22:04:31	2016-09-03 22:04:31	\N
365	44	8	agree	Da, nije u skladu s Bogom	2016-09-03 22:04:31	2016-09-03 22:04:31	\N
366	45	8	neither	-	2016-09-03 22:04:31	2016-09-03 22:04:31	\N
367	46	8	agree	Da	2016-09-03 22:04:31	2016-09-03 22:04:31	\N
368	47	8	neither	-	2016-09-03 22:04:31	2016-09-03 22:04:31	\N
369	48	8	neither	-	2016-09-03 22:04:31	2016-09-03 22:04:31	\N
370	3	9	agree	Da	2016-09-03 22:06:54	2016-09-03 22:06:54	\N
371	4	9	neither	-	2016-09-03 22:06:54	2016-09-03 22:06:54	\N
372	5	9	neither	-	2016-09-03 22:06:54	2016-09-03 22:06:54	\N
373	6	9	agree	Da	2016-09-03 22:06:54	2016-09-03 22:06:54	\N
374	7	9	disagree	Ne, osim ako je vrijednost nekretnine iznad 500.000kn	2016-09-03 22:06:54	2016-09-03 22:06:54	\N
375	8	9	agree	Da, no spominju samo političko oglašavanje	2016-09-03 22:06:54	2016-09-03 22:06:54	\N
376	9	9	agree	Da, revidirati Vatikanske ugovore	2016-09-03 22:06:54	2016-09-03 22:06:54	\N
377	10	9	agree	Da	2016-09-03 22:06:54	2016-09-03 22:06:54	\N
378	11	9	agree	Da	2016-09-03 22:06:54	2016-09-03 22:06:54	\N
379	12	9	disagree	Ne	2016-09-03 22:06:54	2016-09-03 22:06:54	\N
380	13	9	neither	-	2016-09-03 22:06:54	2016-09-03 22:06:54	\N
381	14	9	agree	Da	2016-09-03 22:06:54	2016-09-03 22:06:54	\N
382	15	9	neither	-	2016-09-03 22:06:54	2016-09-03 22:06:54	\N
383	16	9	agree	Da, kroz poticanje umirovljenja službenika koji bi potom radili kao savjetnici za osposobljavanja mladih	2016-09-03 22:06:54	2016-09-03 22:06:54	\N
384	17	9	agree	Da, proširiti razrede i smanjiti stope.	2016-09-03 22:06:54	2016-09-03 22:06:54	\N
385	18	9	disagree	Ne, ali pojednostavljenje uplate doprinosa (jedna uplata na jedan račun)	2016-09-03 22:06:54	2016-09-03 22:06:54	\N
386	19	9	agree	Da, ukoliko postoji domaća alternativa.	2016-09-03 22:06:54	2016-09-03 22:06:54	\N
387	20	9	neither	-	2016-09-03 22:06:54	2016-09-03 22:06:54	\N
388	21	9	disagree	Ne	2016-09-03 22:06:54	2016-09-03 22:06:54	\N
389	22	9	agree	Da, kroz kontrolu službenika.	2016-09-03 22:06:54	2016-09-03 22:06:54	\N
390	23	9	agree	Da	2016-09-03 22:06:54	2016-09-03 22:06:54	\N
391	24	9	disagree	Ne	2016-09-03 22:06:54	2016-09-03 22:06:54	\N
392	25	9	neither	-	2016-09-03 22:06:54	2016-09-03 22:06:54	\N
393	26	9	agree	Da	2016-09-03 22:06:54	2016-09-03 22:06:54	\N
394	27	9	neither	-	2016-09-03 22:06:54	2016-09-03 22:06:54	\N
395	28	9	disagree	Ne, zdravstvo mora biti državno	2016-09-03 22:06:54	2016-09-03 22:06:54	\N
396	29	9	disagree	Ne	2016-09-03 22:06:54	2016-09-03 22:06:54	\N
397	30	9	agree	Da	2016-09-03 22:06:54	2016-09-03 22:06:54	\N
398	31	9	agree	Da, za deficitarna zanimanja	2016-09-03 22:06:54	2016-09-03 22:06:54	\N
399	32	9	disagree	Ne, besplatno obrazovanje za sve	2016-09-03 22:06:54	2016-09-03 22:06:54	\N
400	33	9	neither	-	2016-09-03 22:06:54	2016-09-03 22:06:54	\N
401	34	9	neither	-	2016-09-03 22:06:54	2016-09-03 22:06:54	\N
402	35	9	agree	Da, revidirati Vatikanske ugovore	2016-09-03 22:06:54	2016-09-03 22:06:54	\N
403	36	9	neither	-	2016-09-03 22:06:54	2016-09-03 22:06:54	\N
404	37	9	agree	Da	2016-09-03 22:06:54	2016-09-03 22:06:54	\N
405	38	9	agree	Da (EU), i postati član promatrač	2016-09-03 22:06:54	2016-09-03 22:06:54	\N
406	39	9	neither	-	2016-09-03 22:06:54	2016-09-03 22:06:54	\N
407	40	9	neither	-	2016-09-03 22:06:54	2016-09-03 22:06:54	\N
408	41	9	disagree	Ne	2016-09-03 22:06:54	2016-09-03 22:06:54	\N
409	42	9	agree	Da, ukidanje svih županija i decentralizacija.	2016-09-03 22:06:54	2016-09-03 22:06:54	\N
410	43	9	agree	Da	2016-09-03 22:06:54	2016-09-03 22:06:54	\N
411	44	9	agree	Da	2016-09-03 22:06:54	2016-09-03 22:06:54	\N
412	45	9	neither	-	2016-09-03 22:06:54	2016-09-03 22:06:54	\N
413	46	9	agree	Da	2016-09-03 22:06:54	2016-09-03 22:06:54	\N
414	47	9	neither	-	2016-09-03 22:06:54	2016-09-03 22:06:54	\N
415	48	9	agree	Da	2016-09-03 22:06:54	2016-09-03 22:06:54	\N
416	3	10	agree	Da	2016-09-03 22:09:02	2016-09-03 22:09:02	\N
417	4	10	agree	Da	2016-09-03 22:09:02	2016-09-03 22:09:02	\N
418	5	10	agree	Da, ali ne treba definirati njihova prava Ustavom.	2016-09-03 22:09:02	2016-09-03 22:09:02	\N
419	6	10	neither	-	2016-09-03 22:09:02	2016-09-03 22:09:02	\N
420	7	10	disagree	Ne	2016-09-03 22:09:02	2016-09-03 22:09:02	\N
421	8	10	neither	-	2016-09-03 22:09:02	2016-09-03 22:09:02	\N
422	9	10	disagree	Ne, ne treba mijenjati vatikanske ugovore.	2016-09-03 22:09:02	2016-09-03 22:09:02	\N
423	10	10	neither	-	2016-09-03 22:09:02	2016-09-03 22:09:02	\N
424	11	10	agree	Da	2016-09-03 22:09:02	2016-09-03 22:09:02	\N
425	12	10	neither	-	2016-09-03 22:09:02	2016-09-03 22:09:02	\N
426	13	10	agree	Da (3.500 kn)	2016-09-03 22:09:02	2016-09-03 22:09:02	\N
427	14	10	neither	-	2016-09-03 22:09:02	2016-09-03 22:09:02	\N
428	15	10	agree	Da, "pošten standard" za sve	2016-09-03 22:09:02	2016-09-03 22:09:02	\N
429	16	10	agree	Da	2016-09-03 22:09:02	2016-09-03 22:09:02	\N
430	17	10	disagree	Ne, i povećati poreze za plaće duplo veće od prosječne, ali smanjiti za ispodprosječne.	2016-09-03 22:09:02	2016-09-03 22:09:02	\N
431	18	10	agree	Da, smanjiti doprinose na plaće.	2016-09-03 22:09:02	2016-09-03 22:09:02	\N
432	19	10	neither	-	2016-09-03 22:09:02	2016-09-03 22:09:02	\N
433	20	10	neither	-	2016-09-03 22:09:02	2016-09-03 22:09:02	\N
434	21	10	agree	Da	2016-09-03 22:09:02	2016-09-03 22:09:02	\N
435	22	10	agree	Da	2016-09-03 22:09:02	2016-09-03 22:09:02	\N
436	23	10	neither	-	2016-09-03 22:09:02	2016-09-03 22:09:02	\N
437	24	10	disagree	Ne	2016-09-03 22:09:02	2016-09-03 22:09:02	\N
438	25	10	neither	-	2016-09-03 22:09:02	2016-09-03 22:09:02	\N
439	26	10	neither	-	2016-09-03 22:09:02	2016-09-03 22:09:02	\N
440	27	10	neither	-	2016-09-03 22:09:02	2016-09-03 22:09:02	\N
441	28	10	disagree	Ne	2016-09-03 22:09:02	2016-09-03 22:09:02	\N
442	29	10	agree	Da, dokle god je pošteno i transparentno (imovinski cenzus)	2016-09-03 22:09:02	2016-09-03 22:09:02	\N
443	30	10	agree	Da	2016-09-03 22:09:02	2016-09-03 22:09:02	\N
444	31	10	neither	-	2016-09-03 22:09:02	2016-09-03 22:09:02	\N
445	32	10	neither	-	2016-09-03 22:09:02	2016-09-03 22:09:02	\N
446	33	10	agree	Da, osigurati besplatne udžbenike i prijevoz za sve učenike.	2016-09-03 22:09:02	2016-09-03 22:09:02	\N
447	34	10	neither	-	2016-09-03 22:09:02	2016-09-03 22:09:02	\N
448	35	10	disagree	Ne, ne treba mijenjati vatikanske ugovore.	2016-09-03 22:09:02	2016-09-03 22:09:02	\N
449	36	10	neither	-	2016-09-03 22:09:02	2016-09-03 22:09:02	\N
450	37	10	agree	Da	2016-09-03 22:09:02	2016-09-03 22:09:02	\N
451	38	10	disagree	Ne	2016-09-03 22:09:02	2016-09-03 22:09:02	\N
452	39	10	neither	-	2016-09-03 22:09:02	2016-09-03 22:09:02	\N
453	40	10	disagree	Ne	2016-09-03 22:09:02	2016-09-03 22:09:02	\N
455	42	10	agree	Da	2016-09-03 22:09:02	2016-09-03 22:09:02	\N
456	43	10	neither	-	2016-09-03 22:09:02	2016-09-03 22:09:02	\N
457	44	10	neither	-	2016-09-03 22:09:02	2016-09-03 22:09:02	\N
458	45	10	neither	-	2016-09-03 22:09:02	2016-09-03 22:09:02	\N
459	46	10	agree	Da	2016-09-03 22:09:02	2016-09-03 22:09:02	\N
460	47	10	neither	-	2016-09-03 22:09:02	2016-09-03 22:09:02	\N
461	48	10	neither	-	2016-09-03 22:09:02	2016-09-03 22:09:02	\N
462	3	11	agree	Da	2016-09-03 22:15:03	2016-09-03 22:15:03	\N
463	4	11	neither	-	2016-09-03 22:15:03	2016-09-03 22:15:03	\N
464	5	11	neither	-	2016-09-03 22:15:03	2016-09-03 22:15:03	\N
465	6	11	agree	Da	2016-09-03 22:15:03	2016-09-03 22:15:03	\N
466	7	11	disagree	Ne	2016-09-03 22:15:03	2016-09-03 22:15:03	\N
467	8	11	agree	Da	2016-09-03 22:15:03	2016-09-03 22:15:03	\N
468	9	11	agree	Da	2016-09-03 22:15:03	2016-09-03 22:15:03	\N
469	10	11	neither	-	2016-09-03 22:15:03	2016-09-03 22:15:03	\N
470	11	11	neither	-	2016-09-03 22:15:03	2016-09-03 22:15:03	\N
471	12	11	neither	-	2016-09-03 22:15:03	2016-09-03 22:15:03	\N
472	13	11	neither	-	2016-09-03 22:15:03	2016-09-03 22:15:03	\N
473	14	11	neither	-	2016-09-03 22:15:03	2016-09-03 22:15:03	\N
474	15	11	neither	-	2016-09-03 22:15:03	2016-09-03 22:15:03	\N
475	16	11	neither	-	2016-09-03 22:15:03	2016-09-03 22:15:03	\N
476	17	11	disagree	Ne	2016-09-03 22:15:03	2016-09-03 22:15:03	\N
477	18	11	disagree	Ne, i uvesti nove.	2016-09-03 22:15:03	2016-09-03 22:15:03	\N
478	19	11	neither	-	2016-09-03 22:15:03	2016-09-03 22:15:03	\N
479	20	11	neither	-	2016-09-03 22:15:03	2016-09-03 22:15:03	\N
480	21	11	disagree	Ne, i štititi postojeće.	2016-09-03 22:15:03	2016-09-03 22:15:03	\N
481	22	11	agree	Da, prepoloviti	2016-09-03 22:15:03	2016-09-03 22:15:03	\N
482	23	11	neither	-	2016-09-03 22:15:03	2016-09-03 22:15:03	\N
483	24	11	disagree	Ne	2016-09-03 22:15:03	2016-09-03 22:15:03	\N
484	25	11	neither	-	2016-09-03 22:15:03	2016-09-03 22:15:03	\N
485	26	11	neither	-	2016-09-03 22:15:03	2016-09-03 22:15:03	\N
486	27	11	neither	-	2016-09-03 22:15:03	2016-09-03 22:15:03	\N
487	28	11	neither	-	2016-09-03 22:15:03	2016-09-03 22:15:03	\N
488	29	11	neither	-	2016-09-03 22:15:03	2016-09-03 22:15:03	\N
489	30	11	neither	-	2016-09-03 22:15:03	2016-09-03 22:15:03	\N
490	31	11	disagree	Ne	2016-09-03 22:15:03	2016-09-03 22:15:03	\N
491	32	11	neither	-	2016-09-03 22:15:03	2016-09-03 22:15:03	\N
492	33	11	neither	-	2016-09-03 22:15:03	2016-09-03 22:15:03	\N
493	34	11	neither	-	2016-09-03 22:15:03	2016-09-03 22:15:03	\N
494	35	11	neither	-	2016-09-03 22:15:03	2016-09-03 22:15:03	\N
495	36	11	neither	-	2016-09-03 22:15:03	2016-09-03 22:15:03	\N
496	37	11	neither	-	2016-09-03 22:15:03	2016-09-03 22:15:03	\N
497	38	11	neither	-	2016-09-03 22:15:03	2016-09-03 22:15:03	\N
498	39	11	neither	-	2016-09-03 22:15:03	2016-09-03 22:15:03	\N
499	40	11	neither	-	2016-09-03 22:15:03	2016-09-03 22:15:03	\N
500	41	11	disagree	Ne	2016-09-03 22:15:03	2016-09-03 22:15:03	\N
501	42	11	disagree	Ne, ali decentralizirati upravljanje i uvesti direktnu demokraciju.	2016-09-03 22:15:03	2016-09-03 22:15:03	\N
502	43	11	neither	-	2016-09-03 22:15:03	2016-09-03 22:15:03	\N
503	44	11	neither	-	2016-09-03 22:15:03	2016-09-03 22:15:03	\N
504	45	11	neither	-	2016-09-03 22:15:03	2016-09-03 22:15:03	\N
505	46	11	neither	-	2016-09-03 22:15:03	2016-09-03 22:15:03	\N
506	47	11	neither	-	2016-09-03 22:15:03	2016-09-03 22:15:03	\N
507	48	11	neither	-	2016-09-03 22:15:03	2016-09-03 22:15:03	\N
508	3	12	agree	Da	2016-09-03 22:17:03	2016-09-03 22:17:03	\N
509	4	12	agree	Da	2016-09-03 22:17:03	2016-09-03 22:17:03	\N
510	5	12	neither	-	2016-09-03 22:17:03	2016-09-03 22:17:03	\N
511	6	12	neither	-	2016-09-03 22:17:03	2016-09-03 22:17:03	\N
512	7	12	neither	-	2016-09-03 22:17:03	2016-09-03 22:17:03	\N
513	8	12	neither	-	2016-09-03 22:17:03	2016-09-03 22:17:03	\N
514	9	12	neither	-	2016-09-03 22:17:03	2016-09-03 22:17:03	\N
515	10	12	neither	-	2016-09-03 22:17:03	2016-09-03 22:17:03	\N
516	11	12	neither	-	2016-09-03 22:17:03	2016-09-03 22:17:03	\N
517	12	12	neither	-	2016-09-03 22:17:03	2016-09-03 22:17:03	\N
518	13	12	neither	-	2016-09-03 22:17:03	2016-09-03 22:17:03	\N
519	14	12	neither	-	2016-09-03 22:17:03	2016-09-03 22:17:03	\N
520	15	12	neither	-	2016-09-03 22:17:03	2016-09-03 22:17:03	\N
521	16	12	neither	-	2016-09-03 22:17:03	2016-09-03 22:17:03	\N
522	17	12	agree	Da, i smanjiti PDV.	2016-09-03 22:17:03	2016-09-03 22:17:03	\N
523	18	12	agree	Da	2016-09-03 22:17:03	2016-09-03 22:17:03	\N
524	19	12	neither	-	2016-09-03 22:17:03	2016-09-03 22:17:03	\N
525	20	12	neither	-	2016-09-03 22:17:03	2016-09-03 22:17:03	\N
526	21	12	neither	-	2016-09-03 22:17:03	2016-09-03 22:17:03	\N
527	22	12	agree	Da	2016-09-03 22:17:03	2016-09-03 22:17:03	\N
528	23	12	neither	-	2016-09-03 22:17:03	2016-09-03 22:17:03	\N
529	24	12	agree	Da	2016-09-03 22:17:03	2016-09-03 22:17:03	\N
530	25	12	neither	-	2016-09-03 22:17:03	2016-09-03 22:17:03	\N
531	26	12	neither	-	2016-09-03 22:17:03	2016-09-03 22:17:03	\N
532	27	12	neither	-	2016-09-03 22:17:03	2016-09-03 22:17:03	\N
533	28	12	neither	-	2016-09-03 22:17:03	2016-09-03 22:17:03	\N
534	29	12	agree	Da, dokle god je pošteno i transparentno (imovinski cenzus)	2016-09-03 22:17:03	2016-09-03 22:17:03	\N
535	30	12	agree	Da	2016-09-03 22:17:03	2016-09-03 22:17:03	\N
536	31	12	neither	-	2016-09-03 22:17:03	2016-09-03 22:17:03	\N
537	32	12	neither	-	2016-09-03 22:17:03	2016-09-03 22:17:03	\N
538	33	12	neither	-	2016-09-03 22:17:03	2016-09-03 22:17:03	\N
539	34	12	neither	-	2016-09-03 22:17:03	2016-09-03 22:17:03	\N
540	35	12	neither	-	2016-09-03 22:17:03	2016-09-03 22:17:03	\N
541	36	12	neither	-	2016-09-03 22:17:03	2016-09-03 22:17:03	\N
542	37	12	neither	-	2016-09-03 22:17:03	2016-09-03 22:17:03	\N
543	38	12	neither	-	2016-09-03 22:17:03	2016-09-03 22:17:03	\N
544	39	12	neither	-	2016-09-03 22:17:03	2016-09-03 22:17:03	\N
545	40	12	neither	-	2016-09-03 22:17:03	2016-09-03 22:17:03	\N
546	41	12	neither	-	2016-09-03 22:17:03	2016-09-03 22:17:03	\N
547	42	12	disagree	Ne, ali decentralizirati upravljanje.	2016-09-03 22:17:03	2016-09-03 22:17:03	\N
548	43	12	neither	-	2016-09-03 22:17:03	2016-09-03 22:17:03	\N
549	44	12	neither	-	2016-09-03 22:17:03	2016-09-03 22:17:03	\N
550	45	12	neither	-	2016-09-03 22:17:03	2016-09-03 22:17:03	\N
551	46	12	neither	-	2016-09-03 22:17:03	2016-09-03 22:17:03	\N
552	47	12	neither	-	2016-09-03 22:17:03	2016-09-03 22:17:03	\N
553	48	12	neither	-	2016-09-03 22:17:03	2016-09-03 22:17:03	\N
205	22	5	disagree	Ne, povećati kroz javne radove.	2016-09-03 21:49:40	2016-09-03 21:49:40	\N
454	41	10	neither	Implicira se “da” jer Bandić ima optužnicu.	2016-09-03 22:09:02	2016-09-03 22:09:02	\N
\.


--
-- Name: party_answers_id_seq; Type: SEQUENCE SET; Schema: public; Owner: homestead
--

SELECT pg_catalog.setval('party_answers_id_seq', 553, true);


--
-- Data for Name: password_resets; Type: TABLE DATA; Schema: public; Owner: homestead
--

COPY password_resets (email, token, created_at) FROM stdin;
\.


--
-- Data for Name: question_categories; Type: TABLE DATA; Schema: public; Owner: homestead
--

COPY question_categories (id, title, color) FROM stdin;
3	Okoliš	green
2	Društvo	teal
4	Ekonomija	red
5	Zdravlje	black
6	Obrazovanje	yellow
7	Vanjska politika	blue
8	Unutarnja politika	aqua
9	Poljoprivreda, ribolov, šumarstvo	green
\.


--
-- Name: question_categories_id_seq; Type: SEQUENCE SET; Schema: public; Owner: homestead
--

SELECT pg_catalog.setval('question_categories_id_seq', 9, true);


--
-- Data for Name: questions; Type: TABLE DATA; Schema: public; Owner: homestead
--

COPY questions (id, question, description, category_id, created_at, updated_at, deleted_at) FROM stdin;
3	LGBT osobe i parovi trebaju imati ista prava kao i heteroseksualne osobe.	\N	2	2016-09-03 20:41:55	2016-09-03 20:41:55	\N
4	Svaka žena treba imati pravo na pobačaj.	\N	2	2016-09-03 20:45:37	2016-09-03 20:45:51	\N
5	Ratni veterani trebaju imati privilegirani status.	\N	2	2016-09-03 20:45:56	2016-09-03 20:45:56	\N
6	Ovršni zakon treba mijenjati.	\N	2	2016-09-03 20:46:02	2016-09-03 20:46:02	\N
7	Deložacija iz jedine nekretnine treba biti dozvoljena.	\N	2	2016-09-03 20:46:05	2016-09-03 20:46:05	\N
8	HRT treba biti neovisni medij.	\N	2	2016-09-03 20:46:08	2016-09-03 20:46:08	\N
9	Država i Crkva trebaju biti odvojeni.	\N	2	2016-09-03 20:46:11	2016-09-03 20:46:11	\N
10	Potrebno je više regulacija kako bi se spriječile klimatske promjene.	\N	3	2016-09-03 20:47:15	2016-09-03 20:47:15	\N
11	Potrebno je promovirati "zelenu tehnologiju" državnim poticajima.	\N	3	2016-09-03 20:47:15	2016-09-03 20:47:15	\N
12	Treba tražiti i eventualno vaditi naftu iz Jadrana.	\N	3	2016-09-03 20:47:15	2016-09-03 20:47:15	\N
13	Minimalnu plaću treba povećati.	\N	4	2016-09-03 20:48:01	2016-09-03 20:48:01	\N
14	Država treba novčano poticati mala i srednja poduzeća.	\N	4	2016-09-03 20:48:01	2016-09-03 20:48:01	\N
15	Treba uvesti temeljni dohodak ili zajamčeni minimalni standard.	\N	4	2016-09-03 20:48:01	2016-09-03 20:48:01	\N
16	Treba poticati zapošljavanje mladih (pozitivna diskriminacija).	\N	4	2016-09-03 20:48:01	2016-09-03 20:48:01	\N
17	Porez na dohodak treba smanjiti.	\N	4	2016-09-03 20:48:01	2016-09-03 20:48:01	\N
18	Postojeći porezi i doprinosi za poduzetnike se trebaju smanjiti.	\N	4	2016-09-03 20:48:01	2016-09-03 20:48:01	\N
19	Treba uvesti porez na uvozne proizvode.	\N	4	2016-09-03 20:48:01	2016-09-03 20:48:01	\N
20	Treba podržati trans-pacifičko partnerstvo (TPP, TTIP).	\N	4	2016-09-03 20:48:01	2016-09-03 20:48:01	\N
21	Državnu imovinu (nekretnine i poduzeća) koja nije od strateškog interesa treba prodati.	\N	4	2016-09-03 20:48:01	2016-09-03 20:48:01	\N
22	Broj državnih službenika i ostalih državnih zaposlenika treba smanjiti.	\N	4	2016-09-03 20:48:01	2016-09-03 20:48:01	\N
23	Trebamo poticati vanjske investicije.	\N	4	2016-09-03 20:48:01	2016-09-03 20:48:01	\N
24	Trebamo uvesti euro.	\N	4	2016-09-03 20:48:01	2016-09-03 20:48:01	\N
25	Trebamo tražiti sredstva od EU za razvoj.	\N	4	2016-09-03 20:48:01	2016-09-03 20:48:01	\N
26	Konoplju i marihuanu treba legalizirati.	\N	5	2016-09-03 20:49:03	2016-09-03 20:49:03	\N
27	Djecu obavezno treba cijepiti.	\N	5	2016-09-03 20:49:03	2016-09-03 20:49:03	\N
28	Zdravstvo se treba privatizirati.	\N	5	2016-09-03 20:49:03	2016-09-03 20:49:03	\N
29	Za kvalitetno zdravstvo trebamo uvesti naknade za neke zdravstvene usluge.	\N	5	2016-09-03 20:49:03	2016-09-03 20:49:03	\N
30	Cjelovita kurikularna reforma treba ići dalje.	\N	6	2016-09-03 20:49:37	2016-09-03 20:49:37	\N
31	Trebamo poticati znanstvene (STEM) studije i predmete i zanimanja u visokoj potražnji.	\N	6	2016-09-03 20:49:37	2016-09-03 20:49:37	\N
32	Visoko školstvo treba privatizirati.	\N	6	2016-09-03 20:49:37	2016-09-03 20:49:37	\N
33	Naši učenici trebaju dobiti veće stipendije.	\N	6	2016-09-03 20:49:37	2016-09-03 20:49:37	\N
34	Treba uvesti zdravstveni odgoj.	\N	6	2016-09-03 20:49:37	2016-09-03 20:49:37	\N
36	Hrvatska treba blokirati pristup Srbije EU ako ona ne ispuni određene uvjete.	\N	7	2016-09-03 20:50:04	2016-09-03 20:50:04	\N
37	Neregulirani morski resursi trebaju se zaštititi.	\N	7	2016-09-03 20:50:04	2016-09-03 20:50:04	\N
38	Trebamo napustiti EU i NATO savez.	\N	7	2016-09-03 20:50:04	2016-09-03 20:50:04	\N
39	Trebamo bezuvjetno prihvatiti izbjeglice iz Sirije i ostalih ratnih područja.	\N	7	2016-09-03 20:50:04	2016-09-03 20:50:04	\N
40	Moramo uvesti obavezan vojni rok.	\N	7	2016-09-03 20:50:04	2016-09-03 20:50:04	\N
41	Političarima s optužnicama ili presudama treba dozvoliti sudjelovanje na izborima.	\N	8	2016-09-03 20:50:40	2016-09-03 20:50:40	\N
42	Trebamo smanjiti broj općina i županija.	\N	8	2016-09-03 20:50:40	2016-09-03 20:50:40	\N
43	Država treba biti informatizirana a birokratski poslovi moraju se moći obaviti preko interneta.	\N	8	2016-09-03 20:50:40	2016-09-03 20:50:40	\N
44	Treba zabraniti laboratorijski genetski modificirane organizme.	\N	9	2016-09-03 20:51:08	2016-09-03 20:51:08	\N
46	Država mora davati potpore poljoprivrednicima.	\N	9	2016-09-03 20:51:08	2016-09-03 20:51:08	\N
47	Porezi i davanja za poljoprivrednike trebaju biti manje.	\N	9	2016-09-03 20:51:08	2016-09-03 20:51:08	\N
48	Poljoprivredno zemljište se treba lakše moći okrupniti.	\N	9	2016-09-03 20:51:08	2016-09-03 20:51:08	\N
45	Mora se uvesti kategorija ribolova "na malo".	\N	9	2016-09-03 20:51:08	2016-09-03 20:51:08	\N
35	Vjeronauk treba izbaciti iz škola.	\N	6	2016-09-03 20:49:37	2016-09-03 20:49:37	\N
\.


--
-- Name: questions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: homestead
--

SELECT pg_catalog.setval('questions_id_seq', 48, true);


--
-- Data for Name: user_answers; Type: TABLE DATA; Schema: public; Owner: homestead
--

COPY user_answers (user_response_id, question_id, answer) FROM stdin;
\.


--
-- Data for Name: user_responses; Type: TABLE DATA; Schema: public; Owner: homestead
--

COPY user_responses (id, user_id, ip, created_at, updated_at, electoral_unit, voting_for, top_result, result_percentage) FROM stdin;
\.


--
-- Name: user_responses_id_seq; Type: SEQUENCE SET; Schema: public; Owner: homestead
--

SELECT pg_catalog.setval('user_responses_id_seq', 19, true);


--
-- Data for Name: user_results; Type: TABLE DATA; Schema: public; Owner: homestead
--

COPY user_results (id, response_id, party_id, percentage, count, agrees, created_at, updated_at) FROM stdin;
\.


--
-- Name: user_results_id_seq; Type: SEQUENCE SET; Schema: public; Owner: homestead
--

SELECT pg_catalog.setval('user_results_id_seq', 20, true);


--
-- Data for Name: users; Type: TABLE DATA; Schema: public; Owner: homestead
--

COPY users (id, name, email, facebook_id, avatar, remember_token, created_at, updated_at) FROM stdin;
\.


--
-- Name: users_id_seq; Type: SEQUENCE SET; Schema: public; Owner: homestead
--

SELECT pg_catalog.setval('users_id_seq', 4, true);


--
-- Name: parties_pkey; Type: CONSTRAINT; Schema: public; Owner: homestead
--

ALTER TABLE ONLY parties
    ADD CONSTRAINT parties_pkey PRIMARY KEY (id);


--
-- Name: party_answers_party_id_question_id_key; Type: CONSTRAINT; Schema: public; Owner: homestead
--

ALTER TABLE ONLY party_answers
    ADD CONSTRAINT party_answers_party_id_question_id_key UNIQUE (party_id, question_id);


--
-- Name: party_answers_pkey; Type: CONSTRAINT; Schema: public; Owner: homestead
--

ALTER TABLE ONLY party_answers
    ADD CONSTRAINT party_answers_pkey PRIMARY KEY (id);


--
-- Name: question_categories_pkey; Type: CONSTRAINT; Schema: public; Owner: homestead
--

ALTER TABLE ONLY question_categories
    ADD CONSTRAINT question_categories_pkey PRIMARY KEY (id);


--
-- Name: questions_pkey; Type: CONSTRAINT; Schema: public; Owner: homestead
--

ALTER TABLE ONLY questions
    ADD CONSTRAINT questions_pkey PRIMARY KEY (id);


--
-- Name: user_answers_pkey; Type: CONSTRAINT; Schema: public; Owner: homestead
--

ALTER TABLE ONLY user_answers
    ADD CONSTRAINT user_answers_pkey PRIMARY KEY (user_response_id, question_id);


--
-- Name: user_responses_pkey; Type: CONSTRAINT; Schema: public; Owner: homestead
--

ALTER TABLE ONLY user_responses
    ADD CONSTRAINT user_responses_pkey PRIMARY KEY (id);


--
-- Name: user_results_pkey; Type: CONSTRAINT; Schema: public; Owner: homestead
--

ALTER TABLE ONLY user_results
    ADD CONSTRAINT user_results_pkey PRIMARY KEY (id);


--
-- Name: user_results_response_id_party_id_unique; Type: CONSTRAINT; Schema: public; Owner: homestead
--

ALTER TABLE ONLY user_results
    ADD CONSTRAINT user_results_response_id_party_id_unique UNIQUE (response_id, party_id);


--
-- Name: users_email_unique; Type: CONSTRAINT; Schema: public; Owner: homestead
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_email_unique UNIQUE (email);


--
-- Name: users_facebook_id_unique; Type: CONSTRAINT; Schema: public; Owner: homestead
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_facebook_id_unique UNIQUE (facebook_id);


--
-- Name: users_pkey; Type: CONSTRAINT; Schema: public; Owner: homestead
--

ALTER TABLE ONLY users
    ADD CONSTRAINT users_pkey PRIMARY KEY (id);


--
-- Name: password_resets_email_index; Type: INDEX; Schema: public; Owner: homestead
--

CREATE INDEX password_resets_email_index ON password_resets USING btree (email);


--
-- Name: password_resets_token_index; Type: INDEX; Schema: public; Owner: homestead
--

CREATE INDEX password_resets_token_index ON password_resets USING btree (token);


--
-- Name: party_answers_party_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: homestead
--

ALTER TABLE ONLY party_answers
    ADD CONSTRAINT party_answers_party_id_foreign FOREIGN KEY (party_id) REFERENCES parties(id) ON DELETE CASCADE;


--
-- Name: party_answers_question_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: homestead
--

ALTER TABLE ONLY party_answers
    ADD CONSTRAINT party_answers_question_id_foreign FOREIGN KEY (question_id) REFERENCES questions(id) ON DELETE CASCADE;


--
-- Name: questions_category_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: homestead
--

ALTER TABLE ONLY questions
    ADD CONSTRAINT questions_category_id_foreign FOREIGN KEY (category_id) REFERENCES question_categories(id) ON DELETE CASCADE;


--
-- Name: user_answers_question_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: homestead
--

ALTER TABLE ONLY user_answers
    ADD CONSTRAINT user_answers_question_id_foreign FOREIGN KEY (question_id) REFERENCES questions(id) ON DELETE CASCADE;


--
-- Name: user_answers_user_response_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: homestead
--

ALTER TABLE ONLY user_answers
    ADD CONSTRAINT user_answers_user_response_id_foreign FOREIGN KEY (user_response_id) REFERENCES user_responses(id) ON DELETE CASCADE;


--
-- Name: user_responses_top_result_foreign; Type: FK CONSTRAINT; Schema: public; Owner: homestead
--

ALTER TABLE ONLY user_responses
    ADD CONSTRAINT user_responses_top_result_foreign FOREIGN KEY (top_result) REFERENCES parties(id) ON DELETE CASCADE;


--
-- Name: user_responses_user_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: homestead
--

ALTER TABLE ONLY user_responses
    ADD CONSTRAINT user_responses_user_id_foreign FOREIGN KEY (user_id) REFERENCES users(id) ON DELETE CASCADE;


--
-- Name: user_responses_voting_for_foreign; Type: FK CONSTRAINT; Schema: public; Owner: homestead
--

ALTER TABLE ONLY user_responses
    ADD CONSTRAINT user_responses_voting_for_foreign FOREIGN KEY (voting_for) REFERENCES parties(id) ON DELETE CASCADE;


--
-- Name: user_results_response_id_foreign; Type: FK CONSTRAINT; Schema: public; Owner: homestead
--

ALTER TABLE ONLY user_results
    ADD CONSTRAINT user_results_response_id_foreign FOREIGN KEY (response_id) REFERENCES user_responses(id) ON DELETE CASCADE;


--
-- Name: public; Type: ACL; Schema: -; Owner: postgres
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM postgres;
GRANT ALL ON SCHEMA public TO postgres;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

