<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserAnswers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create("user_answers", function (Blueprint $table) {

          $table->integer("user_response_id");
          $table->integer("question_id");
          $table->enum("answer", ['agree', 'disagree', 'neither']);

          $table->foreign("question_id")
            ->references("id")->on("questions")
            ->onDelete("cascade");

          $table->foreign("user_response_id")
            ->references("id")->on("user_responses")
            ->onDelete("cascade");

          $table->primary(["user_response_id", "question_id"]);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("user_answers");
    }
}
