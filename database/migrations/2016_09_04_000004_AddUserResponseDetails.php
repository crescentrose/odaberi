<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserResponseDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::table("user_responses", function(Blueprint $table) {
        $table->integer("electoral_unit")->nullable();
        $table->integer("voting_for")->nullable();
        $table->integer("top_result")->nullable();
        $table->float("result_percentage")->nullable();

        $table->foreign("voting_for")
          ->references("id")->on("parties")
          ->onDelete("cascade");

        $table->foreign("top_result")
          ->references("id")->on("parties")
          ->onDelete("cascade");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
