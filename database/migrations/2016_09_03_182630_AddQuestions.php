<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddQuestions extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create("questions", function(Blueprint $table) {
        $table->increments("id");
        $table->string("question");
        $table->text("description")->nullable();
        $table->integer("category_id");
        $table->timestamps();
        $table->softDeletes();

        $table->foreign("category_id")
          ->references("id")->on("question_categories")
          ->onDelete("cascade");
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("questions");
    }
}
