<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddUserResults extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create("user_results", function(Blueprint $table) {
        $table->increments("id");
        $table->integer("response_id");
        $table->integer("party_id");
        $table->float("percentage");
        $table->integer("count");
        $table->integer("agrees");
        $table->timestamps();

        $table->foreign("response_id")
          ->references("id")->on("user_responses")
          ->onDelete("cascade");

        $table->unique(['response_id', 'party_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
