<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AddPartyAnswers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
      Schema::create("party_answers", function(Blueprint $table) {
        $table->increments("id");
        $table->integer("question_id");
        $table->integer("party_id");
        $table->enum("answer", ['agree', 'disagree', 'neither']);
        $table->string("detail")->nullable();
        $table->timestamps();
        $table->softDeletes();

        $table->foreign("question_id")
          ->references("id")->on("questions")
          ->onDelete("cascade");

        $table->foreign("party_id")
          ->references("id")->on("parties")
          ->onDelete("cascade");
      });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop("party_answers");
    }
}
